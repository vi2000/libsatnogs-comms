/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "bsp/bsp.hpp"
#include "etl_profile.h"
#include "version.h"
#include <errno.h>
#include <satnogs-comms/board.hpp>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/i2c.h>
#include <zephyr/drivers/spi.h>
#include <zephyr/kernel.h>
#include <zephyr/stats/stats.h>
#include <zephyr/sys/reboot.h>
#include <zephyr/sys/util.h>
#include <zephyr/task_wdt/task_wdt.h>

namespace sc = satnogs::comms;

#if DT_HAS_COMPAT_STATUS_OKAY(st_stm32_watchdog)
#define WDT_NODE DT_COMPAT_GET_ANY_STATUS_OKAY(st_stm32_watchdog)
#else
#error "IWDG is not available. Check the device tree configuration"
#endif

const struct gpio_dt_spec led0 =
    GPIO_DT_SPEC_GET_BY_IDX(DT_ALIAS(led0), gpios, 0);
const struct gpio_dt_spec led1 =
    GPIO_DT_SPEC_GET_BY_IDX(DT_ALIAS(led1), gpios, 0);

const struct spi_config radio_spi_cfg = {
    .frequency = 5000000,
    .operation = SPI_OP_MODE_MASTER | SPI_TRANSFER_MSB | SPI_WORD_SET(8) |
                 SPI_LINES_SINGLE | SPI_FULL_DUPLEX | SPI_FRAME_FORMAT_MOTOROLA,
    .cs = {.gpio  = SPI_CS_GPIOS_DT_SPEC_GET(DT_NODELABEL(radio_spi)),
           .delay = 200}};
const struct device *radio_spi_dev = DEVICE_DT_GET(DT_NODELABEL(spi4));

const struct device *i2c_dev = DEVICE_DT_GET(DT_NODELABEL(i2c4));

const struct device *adc1 = DEVICE_DT_GET(DT_NODELABEL(adc1));
const struct device *adc3 = DEVICE_DT_GET(DT_NODELABEL(adc3));

spi_bsp radio_spi(radio_spi_dev, radio_spi_cfg);

K_THREAD_STACK_DEFINE(workqueue_thread_stack, 1024);
struct k_work_q main_work_q;

// static void
// task_wdt_callback(int channel_id, void *user_data)
// {
//   printk("Task watchdog channel %d callback", channel_id);

//   /*
//    * If the issue could be resolved, call task_wdt_feed(channel_id) here
//    * to continue operation.
//    *
//    * Otherwise we can perform some cleanup and reset the device.
//    */

//   sys_reboot(SYS_REBOOT_COLD);
// }

gpio_bsp   l0(&led0);
gpio_bsp   l1(&led1);
chrono_bsp chr;
adc_bsp    adc(nullptr, 0);
adc_bsp    adc_imon_3v3_d(adc1, sc::power_v0_3::IMOV_3V3_D_ADC_CH);
adc_bsp    adc_imon_5v_rf(adc3, sc::power_v0_3::IMOV_5V_RF_ADC_CH);
adc_bsp    adc_imon_fpga(adc3, sc::power_v0_3::IMOV_5V_FPGA_ADC_CH);
dac_bsp    dac1(nullptr, 0);
i2c_bsp    sensors_i2c(i2c_dev);
gpio_bsp   x(nullptr);

int
main(void)
{
  /* Handling different IO of the power subsystem */
  auto pwr_cnf = []() -> auto {
    if constexpr (sc::version::hw() > sc::version::num(0, 2)) {
      sc::power_v0_3::i_lim   current_limit(0.596f, 0.949f, 0.596f);
      sc::power_v0_3::io_conf cnf = {.mon_i2c       = sensors_i2c,
                                     .rf_5v_en      = x,
                                     .fpga_5v_en    = x,
                                     .can1_en       = x,
                                     .can1_low_pwr  = x,
                                     .can2_en       = x,
                                     .can2_low_pwr  = x,
                                     .rf_5v_pgood   = x,
                                     .fpga_5v_pgood = x,
                                     .uhf_en        = x,
                                     .uhf_pgood     = x,
                                     .sband_en      = x,
                                     .sband_pgood   = x,
                                     .imon_3v3_d    = adc_imon_3v3_d,
                                     .imon_5v_rf    = adc_imon_5v_rf,
                                     .imon_fpga     = adc_imon_fpga,
                                     .ilim          = current_limit};

      return cnf;
    } else {
      sc::power_v0_2::io_conf cnf = {.mon_i2c       = sensors_i2c,
                                     .rf_5v_en      = x,
                                     .fpga_5v_en    = x,
                                     .can1_en       = x,
                                     .can1_low_pwr  = x,
                                     .can2_en       = x,
                                     .can2_low_pwr  = x,
                                     .rf_5v_pgood   = x,
                                     .rf_3v3_pgood  = x,
                                     .fpga_5v_pgood = x};
      return cnf;
    }
  }();

  sc::rf_frontend09::io_conf rffe09_io = {
      .en_agc = x, .agc_vset = dac1, .flt_sel = x};

  sc::rf_frontend24::io_conf rffe24_io = {.en_agc    = x,
                                          .agc_vset  = dac1,
                                          .flt_sel   = x,
                                          .mixer_clk = x,
                                          .mixer_rst = x,
                                          .mixer_enx = x,
                                          .mixer_sda = x,
                                          .chrono    = chr};

  sc::radio::io_conf radio_cnf = {.spi_ctrl  = radio_spi,
                                  .nreset    = x,
                                  .chrono    = chr,
                                  .rffe09_io = rffe09_io,
                                  .rffe24_io = rffe24_io};

  sc::board::io_conf board_io = {.emmc_en          = x,
                                 .emmc_sel         = x,
                                 .fpga_spi         = radio_spi,
                                 .led0             = l0,
                                 .led1             = l1,
                                 .pwr_io           = pwr_cnf,
                                 .sensors_i2c      = sensors_i2c,
                                 .alert_t_pa_uhf   = x,
                                 .alert_t_pa_sband = x,
                                 .radio_io         = radio_cnf,
                                 .chrono           = chr};

  msgq<sc::radio::rx_msg, 2> rx_msgq;

  sc::board::params board_params = {.fpga_hw = sc::fpga::hw::ALINX_AC7Z020,
                                    .emmc_capacity = 8ULL * 1024 * 1024 * 1024,
                                    .emmc_start    = 0,
                                    .emmc_stop     = 1024 * 100,
                                    .rx_msgq       = rx_msgq};

  sc::board::init(board_io, board_params);

  auto &radio = sc::board::get_instance().radio();

  radio.enable();
  radio.enable(sc::radio::interface::UHF, false);
  radio.enable(sc::radio::interface::SBAND, false);

  auto &fpga = sc::board::get_instance().fpga();
  fpga.enable();

  const struct device *hw_wdt_dev = DEVICE_DT_GET(WDT_NODE);
  if (!device_is_ready(hw_wdt_dev)) {
    printk("Hardware watchdog %s is not ready; ignoring it.\n",
           hw_wdt_dev->name);
    hw_wdt_dev = NULL;
  }

  int ret = task_wdt_init(hw_wdt_dev);
  if (ret) {
    k_oops();
  }

  int wdt_id = task_wdt_add(10000, NULL, NULL);
  /*
   * Initialize the main work queue that can accept workers. This a
   * good practice instead of executing directly stuff into the timer.
   *
   * Timers on Zephyr operate in interrupt context.
   */
  k_work_queue_start(&main_work_q, workqueue_thread_stack,
                     K_THREAD_STACK_SIZEOF(workqueue_thread_stack), 5, NULL);

  while (1) {
    task_wdt_feed(wdt_id);
    k_msleep(1000);
  }
  return 0;
}

/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef BSP_HPP
#define BSP_HPP

#include <satnogs-comms/bsp/adc.hpp>
#include <satnogs-comms/bsp/chrono.hpp>
#include <satnogs-comms/bsp/dac.hpp>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/bsp/i2c.hpp>
#include <satnogs-comms/bsp/msgq.hpp>
#include <satnogs-comms/bsp/spi.hpp>
#include <satnogs-comms/error.hpp>
#include <satnogs-comms/exception.hpp>
#include <zephyr/drivers/adc.h>
#include <zephyr/drivers/dac.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/i2c.h>
#include <zephyr/drivers/spi.h>

class gpio_bsp : public satnogs::comms::bsp::gpio
{
public:
  gpio_bsp(const struct gpio_dt_spec *io) : gpio(), m_gpio(io) {}

  void
  toggle()
  {
    gpio_pin_toggle_dt(m_gpio);
  }

  bool
  get()
  {
    int ret = gpio_pin_get_dt(m_gpio);
    return ret > 0;
  }

  void
  set(bool s)
  {
    gpio_pin_set_dt(m_gpio, s);
  }

  void
  set_direction(direction dir)
  {
    if (dir == satnogs::comms::bsp::gpio::direction::INPUT) {
      gpio_dt_flags_t mask = 0xFFFF;
      mask ^= GPIO_OUTPUT_ACTIVE;
      mask ^= GPIO_OUTPUT;
      gpio_pin_configure_dt(m_gpio, GPIO_INPUT | (m_gpio->dt_flags & mask));
    } else {
      gpio_dt_flags_t mask = 0xFFFF;
      mask ^= GPIO_INPUT;
      gpio_pin_configure_dt(m_gpio, GPIO_OUTPUT | GPIO_OUTPUT_ACTIVE |
                                        (m_gpio->dt_flags & mask));
    }
  }

private:
  const struct gpio_dt_spec *m_gpio;
};

class adc_bsp : public satnogs::comms::bsp::adc
{

public:
  static constexpr uint16_t resolution = 4096;
  static constexpr float vref = 3.3;

  adc_bsp(const struct device *adc_dev, uint32_t channel)
      : adc(resolution, vref), m_adc(adc_dev), m_channel(channel)
  {
  }

  void
  start()
  {
  }

  void
  stop()
  {
  }

  uint32_t
  read()
  {
    static uint16_t            adc_sample;
    static struct adc_sequence seq = {
        .options = nullptr,
        /* individual channels will be added below */
        .channels = 0,
        .buffer   = &adc_sample,
        /* buffer size in bytes, not number of samples */
        .buffer_size  = sizeof(adc_sample),
        .resolution   = 12,
        .oversampling = 0,
        .calibrate    = 0};
    adc_read(m_adc, &seq);
    return adc_sample;
  }

private:
  const struct device *m_adc;
  const uint32_t       m_channel;
};

class dac_bsp : public satnogs::comms::bsp::dac
{
public:
  dac_bsp(const struct device *dac_dev, uint32_t channel)
      : dac(), m_dac(dac_dev), m_channel(channel)
  {
  }

  void
  start()
  {
  }
  void
  stop()
  {
  }
  void
  write(uint32_t x)
  {
  }

private:
  const struct device *m_dac;
  const uint32_t       m_channel;
};

class i2c_bsp_exception : public satnogs::comms::exception
{
public:
  i2c_bsp_exception(string_type file_name, numeric_type line, int err)
      : exception(ETL_ERROR_TEXT("i2c error", "i2cerr"), file_name, line, err)
  {
  }
};

class i2c_bsp : public satnogs::comms::bsp::i2c
{
public:
  i2c_bsp(const struct device *i2c_dev) : i2c(), m_i2c(i2c_dev) {}

  void
  read(uint16_t addr, uint8_t *rx, size_t rxlen, const uint8_t *tx,
       size_t txlen)
  {
    int ret = i2c_write_read(m_i2c, addr, tx, txlen, rx, rxlen);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, i2c_bsp_exception);
  }

  void
  read(uint16_t addr, uint8_t start_addr, uint8_t *rx, size_t len)
  {
    int ret = i2c_burst_read(m_i2c, addr, start_addr, rx, len);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, i2c_bsp_exception);
  }

  void
  write(uint16_t addr, const uint8_t *tx, size_t len)
  {
    int ret = i2c_write(m_i2c, tx, len, addr);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, i2c_bsp_exception);
  }

  void
  write(uint16_t addr, uint8_t start_addr, const uint8_t *tx, size_t len)
  {
    int ret = i2c_burst_write(m_i2c, addr, start_addr, tx, len);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, i2c_bsp_exception);
  }

private:
  const struct device *m_i2c;
};

class spi_bsp_exception : public satnogs::comms::exception
{
public:
  spi_bsp_exception(string_type file_name, numeric_type line, int err)
      : exception(ETL_ERROR_TEXT("SPI error", "spierr"), file_name, line, err)
  {
  }
};

class spi_bsp : public satnogs::comms::bsp::spi
{
public:
  spi_bsp(const struct device *dev, const struct spi_config &spi_cfg)
      : m_dev(dev), m_spi_cfg(spi_cfg)
  {
  }

  void
  read(uint8_t *rx, const uint8_t *tx, size_t tx_len, size_t rx_len)
  {
    struct spi_buf tx_buf = {.buf = const_cast<uint8_t *>(tx), .len = tx_len};
    struct spi_buf rx_buf = {.buf = const_cast<uint8_t *>(rx), .len = rx_len};
    struct spi_buf_set tx_bufs = {
        .buffers = &tx_buf,
        .count   = 1,
    };
    struct spi_buf_set rx_bufs = {
        .buffers = &rx_buf,
        .count   = 1,
    };
    int ret = spi_transceive(m_dev, &m_spi_cfg, &tx_bufs, &rx_bufs);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, spi_bsp_exception);
  }

  void
  write(const uint8_t *tx, size_t len)
  {
    struct spi_buf     tx_buf  = {.buf = const_cast<uint8_t *>(tx), .len = len};
    struct spi_buf_set tx_bufs = {
        .buffers = &tx_buf,
        .count   = 1,
    };
    int ret = spi_transceive(m_dev, &m_spi_cfg, &tx_bufs, nullptr);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, spi_bsp_exception);
  }

private:
  const struct device    *m_dev;
  const struct spi_config m_spi_cfg;
};

class chrono_bsp : public satnogs::comms::bsp::chrono
{
public:
  void
  delay_us(size_t us)
  {
    k_usleep(us);
  }

  int64_t
  time_ms()
  {
    return k_uptime_get();
  }
};

template <typename T, const size_t LEN>
class msgq : public satnogs::comms::bsp::msgq<T, LEN>
{
public:
  msgq() : satnogs::comms::bsp::msgq<T, LEN>()
  {
    k_msgq_init(&m_mq, m_buffer, sizeof(T), LEN);
  }

  size_t
  size()
  {
    return k_msgq_num_used_get(&m_mq);
  }

  int
  put(const T &msg, size_t timeout_ms)
  {
    return k_msgq_put(&m_mq, &msg, K_MSEC(timeout_ms));
  }

  int
  put_isr(const T &msg)
  {
    return k_msgq_put(&m_mq, &msg, K_NO_WAIT);
  }

  int
  peek(T *msg)
  {
    return k_msgq_peek(&m_mq, msg);
  }

  int
  get(T *msg, size_t timeout_ms)
  {
    return k_msgq_get(&m_mq, msg, K_MSEC(timeout_ms));
  }

  int
  get_isr(T *msg)
  {
    return k_msgq_get(&m_mq, msg, K_NO_WAIT);
  }

private:
  struct k_msgq m_mq;
  char          __aligned(4) m_buffer[LEN * sizeof(T)];
};

#endif // BSP_HPP

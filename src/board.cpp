/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/board.hpp>

namespace satnogs::comms
{

static board *m_instance = nullptr;

void
board::init(const io_conf &io, const params &p)
{
  if (m_instance && m_instance->m_init) {
    SATNOGS_COMMS_ERROR(board_initialized_exception);
  }
  static board inst(io, p);
  m_instance         = &inst;
  m_instance->m_init = true;
}

board &
board::get_instance()
{
  if (!m_instance || !m_instance->m_init) {
    SATNOGS_COMMS_ERROR(board_uninitialized_exception);
  }
  return *m_instance;
}

comms::radio &
board::radio()
{
  return m_radio;
}

comms::emmc &
board::emmc()
{
  return m_emmc;
}

comms::power &
board::power()
{
  return m_power;
}

comms::fpga &
board::fpga()
{
  return m_fpga;
}

comms::leds &
board::leds()
{
  return m_leds;
}

bool
board::alert(temperature_sensor s) const
{
  return m_temperature.alert(s);
}

float
board::temperature(temperature_sensor s) const
{
  return m_temperature.get(s);
}

board::board(const io_conf &io, const params &p)
    : m_init(false),
      m_power(io.pwr_io),
      m_radio(io.radio_io, m_power, p.rx_msgq),
      m_fpga(p.fpga_hw, io.fpga_spi, m_power),
      m_emmc(etl::make_string("emmc1"), p.emmc_capacity, p.emmc_start,
             p.emmc_stop, io.emmc_en, io.emmc_sel),
      m_leds(io.led0, io.led1),
      m_temperature(io.sensors_i2c, io.alert_t_pa_uhf, io.alert_t_pa_sband,
                    m_radio.uhf(), m_radio.sband())
{
}

} // namespace satnogs::comms
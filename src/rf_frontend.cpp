/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/error.hpp>
#include <satnogs-comms/rf_frontend.hpp>

namespace satnogs::comms
{

rf_frontend::rf_frontend(const std::pair<float, float> &rx_range,
                         const std::pair<float, float> &tx_range, io_conf &&io,
                         power &pwr)
    : m_rx_freq_range(rx_range),
      m_tx_freq_range(tx_range),
      m_filt_sel(io.flt_sel),
      m_pwr(pwr),
      m_agc(io.en_agc, io.agc_vset),
      m_dir(dir::RX),
      m_gain_mode(gain_mode::MANUAL),
      m_gain(0.0f)
{
  /* By default select the narrow filter */
  set_filter(filter::NARROW);

  /* This will minimize power consumption */
  set_gain_mode(gain_mode::MANUAL);
  set_gain(0.0f);
}

/**
 * Sets the RX hardware filter path
 * @param f the RF filter path (filter::NARROW or filter::WIDE)
 */
void
rf_frontend::set_filter(filter f)
{
  switch (f) {
  case filter::WIDE:
    m_filt_sel.set(false);
    break;
  case filter::NARROW:
    m_filt_sel.set(true);
    break;
  default:
    SATNOGS_COMMS_ERROR(rf_frontend_inval_param_exception);
    break;
  }
}

/**
 *
 * @return the selected RX hardware filter path
 */
rf_frontend::filter
rf_frontend::get_filter() const
{
  if (m_filt_sel.get()) {
    return filter::NARROW;
  }
  return filter::WIDE;
}

void
rf_frontend::set_direction(dir d)
{
  // Only S-Band requires special handling
  switch (d) {
  case dir::RX:
  case dir::TX:
    m_dir = d;
    return;
  default:
    SATNOGS_COMMS_ERROR(rf_frontend_inval_param_exception);
  }
}

rf_frontend::dir
rf_frontend::direction() const
{
  return m_dir;
}

/**
 * @brief Sets the gain mode of the RX chain
 *
 * @param mode the gain mode
 */
void
rf_frontend::set_gain_mode(enum gain_mode mode)
{
  switch (mode) {
  case gain_mode::AUTO:
  case gain_mode::MANUAL:
    m_agc.enable(mode == gain_mode::AUTO);
    m_gain_mode = mode;
    return;
  default:
    SATNOGS_COMMS_ERROR(rf_frontend_inval_param_exception);
  }
}

enum rf_frontend::gain_mode
rf_frontend::gain_mode() const
{
  return m_gain_mode;
}

void
rf_frontend::set_gain(float db)
{
  // FIXME This should be calculated to the correct value
  m_agc.vset(db);
  m_gain = db;
}

float
rf_frontend::gain() const
{
  return m_gain;
}

/**
 * @brief Checks if a frequency is within the permissible range
 *
 * @param d the direction (RX or TX)
 * @param freq the desired frequency
 * @return true if the frequency is within the Permissible range, false
 * otherwise
 */
bool
rf_frontend::frequency_valid(dir d, float freq) const
{
  switch (d) {
  case dir::RX:
    if (freq < m_rx_freq_range.first || freq > m_rx_freq_range.second) {
      return false;
    }
    return true;
  case dir::TX:
    if (freq < m_tx_freq_range.first || freq > m_tx_freq_range.second) {
      return false;
    }
    return true;
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
}

} // namespace satnogs::comms

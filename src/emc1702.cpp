/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */
#include <cmath>
#include <satnogs-comms/emc1702.hpp>

namespace satnogs::comms
{

/* Refer to:
 * EMC1702 datasheet:
 * https://ww1.microchip.com/downloads/aemDocuments/documents/OTH/ProductDocuments/DataSheets/EMC1702-Data-Sheet-DS20006455A.pdf
 *
 * Register
 *  Description can be found in datasheet p. 30 - 33
 */

#define REG_STATUS                            0x34 // Read Only
#define REG_HIGH_LIMIT_STATUS                 0x35 // Read Only
#define REG_LOW_LIMIT_STATUS                  0x36 // Read Only
#define REG_CRIT_LIMIT_STATUS                 0x37 // Read Only
#define REG_INTERNAL_DIODE_HIGH_BYTE          0x38 // Read Only
#define REG_INTERNAL_DIODE_LOW_BYTE           0x39 // Read Only
#define REG_EXTERNAL_DIODE_HIGH_BYTE          0x3A // Read Only
#define REG_EXTERNAL_DIODE_LOW_BYTE           0x3B // Read Only
#define REG_AVERAGING_CONTROL                 0x40 // R/W
#define REG_CONFIGURATION                     0x03 // R/W
#define REG_CONVERSION_RATE                   0x04 // R/W
#define REG_INTERNAL_DIODE_HIGH_LIM           0x05 // R/W
#define REG_INTERNAL_DIODE_LOW_LIM            0x06 // R/W
#define REG_EXTERNAL_DIODE_HIGH_LIM_HIGH_BYTE 0x07 // R/W
#define REG_EXTERNAL_DIODE_LOW_LIM_HIGH_BYTE  0x08 // R/W
#define REG_EXTERNAL_DIODE_HIGH_LIM_LOW_BYTE  0x13 // R/W
#define REG_EXTERNAL_DIODE_LOW_LIM_LOW_BYTE   0x14 // R/W
#define REG_EXTERNAL_DIODE_TCRIT_LIM          0x19 // R/W
#define REG_CHANNEL_MASK_REG                  0x1F // R/W
#define REG_INTERNAL_DIODE_TCRIT_LIM          0x20 // R/W
#define REG_TCRIT_HYSTERISIS                  0x21 // R/W
#define REG_TCRIT_CONSECUTIVE_ALERT           0x22 // R/W
#define REG_EXTERNAL_DIODE_BETA_CONFIG        0x25 // R/W
#define REG_EXTERNAL_DIODE_IDEALITY_FACTOR    0x25 // R/W
#define REG_ONE_SHOT                          0x0F // Write Only
#define REG_EXTERNAL_DIODE_FAULT              0x1B // Read Only
#define REG_PRODUCT_FEATURES                  0xFC // Read Only
#define REG_PRODUCT_ID                        0xFD // Read Only
#define REG_MICROCHIP_ID                      0xFE // Read Only
#define REG_REVISION                          0xFF // Read Only
#define VOLTAGE_SAMPLING_CONFIG               0x50 // R/W
#define CURRENT_SENSE_SAMPLING_CONFIG         0x51 // R/W
#define PEAK_DETECTION_CONFIG                 0x52 // R/W
#define SENCE_VOLTAGE_HIGH_BYTE               0x54 // Read Only
#define SENCE_VOLTAGE_LOW_BYTE                0x55 // Read Only
#define SOURCE_VOLTAGE_HIGH_BYTE              0x58 // Read Only
#define SOURCE_VOLTAGE_LOW_BYTE               0x59 // Read Only
#define POWER_RATIO_VOLTAGE_HIGH_BYTE         0x5B // Read Only
#define POWER_RATIO_VOLTAGE_LOW_BYTE          0x5C // Read Only
#define SENCE_VOLTAGE_HIGH_LIM                0x60 // R/W
#define SENCE_VOLTAGE_LOW_LIM                 0x61 // R/W
#define SOURCE_VOLTAGE_HIGH_LIM               0x64 // R/W
#define SOURCE_VOLTAGE_LOW_LIM                0x65 // R/W
#define SENCE_VOLTAGE_V_CRIT                  0x66 // R/W
#define SOURCE_VOLTAGE_V_CRIT                 0x68 // R/W
#define SENSE_V_CRIT_HYSTERISIS               0x69 // R/W
#define SOURCE_V_CRIT_HYSTERISIS              0x6A // R/W

emc1702::emc1702(const etl::istring &name, bsp::i2c &i2c, uint16_t addr)
    : emc1702(name, i2c, addr, m_dummy)
{
}

emc1702::emc1702(const etl::istring &name, bsp::i2c &i2c, uint16_t addr,
                 bsp::gpio &alert)
    : m_name(name),
      m_i2c(i2c),
      m_addr(addr),
      m_alert(alert),
      mode(sensor_mode::FULLY_ACTIVE),
      rate(conversion_rate::PER_1_SEC_8_MEAS),
      avrg_temp(averaging_control::DISABLED),
      alert_crit(consecutive_alert_diode_fault::OUT_OF_LIM_MEAS_4),
      alert_lim(consecutive_alert_diode_fault::OUT_OF_LIM_MEAS_4),
      beta(beta_config::AUTO),
      i_factor(ideality_factor::IDEALITY_FACTOR_1_0080),
      v_source_spike(consecutive_alert_voltage::OUT_OF_LIM_MEAS_4),
      avrg_v_source(averaging_control::DISABLED),
      v_sense_spike(consecutive_alert_current::OUT_OF_LIM_MEAS_4),
      avrg_i_sense(averaging_control::DISABLED),
      i_sampling(current_sampling_time::S_T_82_MS),
      v_sense_max(max_expected_voltage::CURRENT_SENSOR_RANGE_80_mV),
      thres(peak_detection_threshold::THRESHOLD_85_mV),
      dur(peak_detection_duration::DURATION_5_12_ms),
      range(),
      s()
{
  // Initialise sensor
  set_config(mode);
  set_conversion_rate(rate);
  set_averaging_control(avrg_temp);

  set_int_temp_high_lim();
  set_int_temp_low_lim();
  set_ext_temp_high_lim();
  set_ext_temp_low_lim();
  set_t_crit();

  set_consecutive_alert(alert_crit, alert_lim);
  set_beta_configuration(beta);
  set_ideality_factor(i_factor);

  set_voltage_sampling_config(v_source_spike, avrg_i_sense);
  set_current_sense_sampling_config(v_sense_spike, avrg_i_sense, i_sampling,
                                    v_sense_max);
  set_peak_detection_config(thres, dur);
  set_v_sense_lim();
  set_v_source_lim();
  set_v_crit_lim();
}

// Temperature
void
emc1702::get_measurements(sensor_mode m)
{
  float temperature_internal;
  float temperature_external;
  float power_ratio;
  float sense_current;
  float source_voltage;
  switch (m) {
  case sensor_mode::FULLY_ACTIVE:
    if (get_crit_limit_status()) {
      SATNOGS_COMMS_ERROR(emc1702_thermal_shutdown_needed);
    } else {
      if (get_sensor_info() == 0) {
        SATNOGS_COMMS_ERROR(emc1702_incorrect_sensor_info);
      } else {
        temperature_internal = get_temperature_internal(); // °C
        temperature_external = get_temperature_external(); // °C
        sense_current        = get_sense_current();        // A
        source_voltage       = get_source_voltage();       // V
        power_ratio          = get_power();                // ratio
      }
    }
    break;

  case sensor_mode::CURRENT_ONLY:
    if (get_crit_limit_status()) {
      SATNOGS_COMMS_ERROR(emc1702_thermal_shutdown_needed);
    } else {
      if (get_sensor_info() == 0) {
        SATNOGS_COMMS_ERROR(emc1702_incorrect_sensor_info);
      } else {
        sense_current  = get_sense_current();  // A
        source_voltage = get_source_voltage(); // V
        power_ratio    = get_power();          // ratio
      }
    }
    break;
  case sensor_mode::TEMPERATURE_ONLY:
    if (get_crit_limit_status()) {
      SATNOGS_COMMS_ERROR(emc1702_thermal_shutdown_needed);
    } else {
      if (get_sensor_info() == 0) {
        SATNOGS_COMMS_ERROR(emc1702_incorrect_sensor_info);
      } else {
        temperature_internal = get_temperature_internal(); // °C
        temperature_external = get_temperature_external(); // °C
      }
    }
    break;
  case sensor_mode::STANDBY:
    if (get_crit_limit_status()) {
      SATNOGS_COMMS_ERROR(emc1702_thermal_shutdown_needed);
    } else {
      if (get_sensor_info() == 0) {
        SATNOGS_COMMS_ERROR(emc1702_incorrect_sensor_info);
      } else {
        // You can request measurements whenever you want calling the
        // emc1702::get_one_shot_meas()
      }
    }
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }
  return;
}

float
emc1702::get_temperature_average() const
{
  return (get_temperature_internal() + get_temperature_external()) / 2;
}

float
emc1702::get_temperature_internal() const
{
  uint8_t buf[2] = {0, 0};
  m_i2c.read(m_addr, REG_INTERNAL_DIODE_HIGH_BYTE, buf, 2);
  uint16_t bit_value = ((buf[0] << 8) | buf[1]) >> 5;
  bool     sign      = (bit_value >> 10) != 0;
  if (sign) {
    bit_value = ~bit_value + 1;
  }
  // Convert 7-bit integer part to decimal
  float temp_value = static_cast<float>((bit_value >> 3) & 0x7F);
  temp_value += static_cast<float>(bit_value & 0x07) * 0.125f;
  // Apply sign
  if (sign) {
    temp_value = -temp_value;
  }
  return temp_value;
  // L1 data (in °C)
}

float
emc1702::get_temperature_external() const
{
  uint8_t buf[2] = {0, 0};
  m_i2c.read(m_addr, REG_EXTERNAL_DIODE_HIGH_BYTE, buf, 2);
  uint16_t bit_value = ((buf[0] << 8) | buf[1]) >> 5;
  bool     sign      = (bit_value >> 10) != 0;
  if (sign) {
    bit_value = ~bit_value + 1;
  }
  // Convert 7-bit integer part to decimal
  float temp_value = static_cast<float>((bit_value >> 3) & 0x7F);
  temp_value += static_cast<float>(bit_value & 0x07) * 0.125f;
  // Apply sign
  if (sign) {
    temp_value = -temp_value;
  }
  return temp_value;
  // L1 data (in °C)
}
void
emc1702::get_status(status &s)
{
  s = {.busy = 0, .crit = 0, .diode_fault = 0, .low = 0, .high = 0, .peak = 0};
  uint8_t buf = 0;
  // The Status Register reports general error conditions.
  m_i2c.read(m_addr, REG_STATUS, &buf, 1);
  // The channels met or exceeded their programmed Tcrit or Vcrit limit.
  s.crit = buf & 0b10;
  // External diode fault was detected
  s.diode_fault = buf & (1 << 2);
  // The channels dropped below their low programmed limit
  s.low = buf & (1 << 3);
  // The channels met or exceeded their programmed high limit
  s.high = buf & (1 << 4);
  // the Peak Detector circuitry has detected a current peak that is greater
  // than the programmed threshold for longer than the programmed duration
  s.peak = buf & (1 << 6);
  s.busy = buf & (1 << 7); // the ADCs is currently converting
  return;
}

void
emc1702::set_config(sensor_mode m) const
{
  uint8_t buf = 0;
  // Set Basic Configuration
  switch (m) {
  case sensor_mode::FULLY_ACTIVE:
  case sensor_mode::CURRENT_ONLY:
  case sensor_mode::TEMPERATURE_ONLY:
  case sensor_mode::STANDBY:
    buf = static_cast<uint8_t>(m);
    m_i2c.write(m_addr, REG_CONFIGURATION, &buf, 1);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }

  return;
}

void
emc1702::set_conversion_rate(conversion_rate r) const
{
  uint8_t buf = 0;
  // Set Conversion Rate
  switch (r) {
  case conversion_rate::PER_1_SEC_8_MEAS:
  case conversion_rate::PER_1_SEC_4_MEAS:
  case conversion_rate::PER_1_SEC_2_MEAS:
  case conversion_rate::PER_1_SEC_1_MEAS:
  case conversion_rate::PER_2_SEC_1_MEAS:
  case conversion_rate::PER_4_SEC_1_MEAS:
  case conversion_rate::PER_8_SEC_1_MEAS:
  case conversion_rate::PER_16_SEC_1_MEAS:
    buf = static_cast<uint8_t>(r);
    m_i2c.write(m_addr, REG_CONVERSION_RATE, &buf, 1);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }
  return;
}

void
emc1702::set_int_temp_high_lim(float int_temp_high_lim) const
{
  uint8_t buf = 0;
  // Set Internal Diode High Limit Temperature
  if (int_temp_high_lim >= 127.0f) {
    buf = 0b01111111;
  } else if (int_temp_high_lim <= -128.0f) {
    buf = 0b10000000;
  } else {
    if (int_temp_high_lim >= 0.0f) {
      buf = static_cast<uint8_t>(abs(round(int_temp_high_lim)));
    } else {
      buf = static_cast<uint8_t>(abs(round(int_temp_high_lim)));
      buf = ~buf;
      buf++;
    }
  }
  m_i2c.write(m_addr, REG_INTERNAL_DIODE_HIGH_LIM, &buf, 1);
  return;
}

void
emc1702::set_int_temp_low_lim(float int_temp_low_lim) const
{
  // Set Internal Diode Low Limit Temperature
  uint8_t buf = 0;
  if (int_temp_low_lim >= 127.0f) {
    buf = 0b01111111;
  } else if (int_temp_low_lim <= -128.0f) {
    buf = 0b10000000;
  } else {
    if (int_temp_low_lim >= 0.0f) {
      buf = static_cast<uint8_t>(abs(round(int_temp_low_lim)));
    } else {
      buf = static_cast<uint8_t>(abs(round(int_temp_low_lim)));
      buf = ~buf;
      buf++;
    }
  }
  m_i2c.write(m_addr, REG_INTERNAL_DIODE_LOW_LIM, &buf, 1);
  return;
}

void
emc1702::set_ext_temp_high_lim(float ext_temp_high_lim) const
{
  // Set External Diode High Limit Temperature
  uint8_t buf[2];
  buf[0] = static_cast<uint8_t>(floor(std::abs(ext_temp_high_lim)));
  if (ext_temp_high_lim < 0.0f) {
    buf[0] |= 0x80;
  }
  float floatingPart = std::abs(ext_temp_high_lim) -
                       static_cast<int>(std::abs(ext_temp_high_lim));
  buf[1] = static_cast<int>(floatingPart * 8.0f);
  buf[1] = buf[1] >> 5;
  m_i2c.write(m_addr, REG_EXTERNAL_DIODE_HIGH_LIM_HIGH_BYTE, buf, 2);
  return;
}

void
emc1702::set_ext_temp_low_lim(float ext_temp_low_lim) const
{
  // Set External Diode Low Limit Temperature
  uint8_t buf[2];
  buf[0] = static_cast<uint8_t>(floor(std::abs(ext_temp_low_lim)));
  if (ext_temp_low_lim < 0.0f) {
    buf[0] |= 0x80;
  }
  float floatingPart =
      std::abs(ext_temp_low_lim) - static_cast<int>(std::abs(ext_temp_low_lim));
  buf[1] = static_cast<int>(floatingPart * 8.0f);
  buf[1] = buf[1] >> 5;
  m_i2c.write(m_addr, REG_EXTERNAL_DIODE_LOW_LIM_HIGH_BYTE, buf, 2);
  return;
}

void
emc1702::get_one_shot_meas() const
{
  // Get One shot measurements
  uint8_t buf;
  buf = 0b11111111;
  m_i2c.write(m_addr, REG_ONE_SHOT, &buf, 1);
  return;
}

void
emc1702::set_t_crit(float ext_t_crit, float int_t_crit,
                    float t_crit_hysterisis) const
{
  uint8_t buf;
  // Set External Diode Critical Temperature
  if (ext_t_crit >= 127.0f) {
    buf = 0b01111111;
  } else if (ext_t_crit <= -128.0f) {
    buf = 0b10000000;
  } else {
    if (ext_t_crit >= 0.0f) {
      buf = static_cast<uint8_t>(abs(round(ext_t_crit)));
    } else {
      buf = static_cast<uint8_t>(abs(round(ext_t_crit)));
      buf = ~buf;
      buf++;
    }
  }
  m_i2c.write(m_addr, REG_EXTERNAL_DIODE_TCRIT_LIM, &buf, 1);

  // Set Internal Diode Critical Temperature
  if (int_t_crit >= 127.0f) {
    buf = 0b01111111;
  } else if (int_t_crit <= -128.0f) {
    buf = 0b10000000;
  } else {
    if (int_t_crit >= 0) {
      buf = static_cast<uint8_t>(abs(round(int_t_crit)));
    } else {
      buf = static_cast<uint8_t>(abs(round(int_t_crit)));
      buf = ~buf;
      buf++;
    }
  }
  m_i2c.write(m_addr, REG_INTERNAL_DIODE_TCRIT_LIM, &buf, 1);

  // Set Critical Temperature Hysteresis
  if (t_crit_hysterisis < 0.0f) {
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  } else if (t_crit_hysterisis >= 127.0f) {
    buf = 0b01111111;
  } else {
    buf = static_cast<uint8_t>(abs(round(t_crit_hysterisis)));
  }
  m_i2c.write(m_addr, REG_TCRIT_HYSTERISIS, &buf, 1);
}

bool
emc1702::get_diode_fault() const
{
  // Get diode fault
  // You can get the same information from the status register
  uint8_t buf;
  bool    diode_fault;
  m_i2c.read(m_addr, REG_EXTERNAL_DIODE_FAULT, &buf, 1);
  // If external diode fault was detected
  diode_fault = (buf & 0b10);
  return diode_fault;
}

void
emc1702::set_consecutive_alert(consecutive_alert_diode_fault c,
                               consecutive_alert_diode_fault l) const
{
  // Set how many times an out-of-limit error or diode fault must
  // be detected in consecutive measurements before the interrupt
  // status registers are asserted.
  uint8_t buf;
  switch (l) {
  case consecutive_alert_diode_fault::OUT_OF_LIM_MEAS_1:
  case consecutive_alert_diode_fault::OUT_OF_LIM_MEAS_2:
  case consecutive_alert_diode_fault::OUT_OF_LIM_MEAS_3:
  case consecutive_alert_diode_fault::OUT_OF_LIM_MEAS_4:
    buf = static_cast<uint8_t>(l);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }
  switch (c) {
  case consecutive_alert_diode_fault::OUT_OF_LIM_MEAS_1:
  case consecutive_alert_diode_fault::OUT_OF_LIM_MEAS_2:
  case consecutive_alert_diode_fault::OUT_OF_LIM_MEAS_3:
  case consecutive_alert_diode_fault::OUT_OF_LIM_MEAS_4:
    buf |= (static_cast<uint8_t>(c) << 3);
    m_i2c.write(m_addr, REG_TCRIT_CONSECUTIVE_ALERT, &buf, 1);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }
  return;
}

void
emc1702::set_beta_configuration(beta_config b) const
{
  // Set beta configuration
  uint8_t buf = 0;
  switch (b) {
  case beta_config::AUTO:
  case beta_config::DISABLED:
  case beta_config::MIN_BETA_0_050:
  case beta_config::MIN_BETA_0_066:
  case beta_config::MIN_BETA_0_087:
  case beta_config::MIN_BETA_0_114:
  case beta_config::MIN_BETA_0_150:
  case beta_config::MIN_BETA_0_197:
  case beta_config::MIN_BETA_0_260:
  case beta_config::MIN_BETA_0_342:
  case beta_config::MIN_BETA_0_449:
  case beta_config::MIN_BETA_0_591:
  case beta_config::MIN_BETA_0_778:
  case beta_config::MIN_BETA_1_024:
  case beta_config::MIN_BETA_1_348:
  case beta_config::MIN_BETA_1_773:
  case beta_config::MIN_BETA_2_333:
    buf = static_cast<uint8_t>(b);
    m_i2c.write(m_addr, REG_EXTERNAL_DIODE_BETA_CONFIG, &buf, 1);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }
  return;
}

void
emc1702::set_ideality_factor(ideality_factor i) const
{
  uint8_t buf;
  // Set ideality factor
  switch (i) {
  case ideality_factor::IDEALITY_FACTOR_1_0053:
  case ideality_factor::IDEALITY_FACTOR_1_0066:
  case ideality_factor::IDEALITY_FACTOR_1_0080:
  case ideality_factor::IDEALITY_FACTOR_1_0093:
  case ideality_factor::IDEALITY_FACTOR_1_0106:
  case ideality_factor::IDEALITY_FACTOR_1_0119:
  case ideality_factor::IDEALITY_FACTOR_1_0133:
  case ideality_factor::IDEALITY_FACTOR_1_0146:
    buf = static_cast<uint8_t>(i);
    m_i2c.write(m_addr, REG_EXTERNAL_DIODE_IDEALITY_FACTOR, &buf, 1);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }
  return;
}

etl::vector<bool, 4>
emc1702::get_high_limit_status() const
{
  // Get the status bits that are set when a temperature or voltage channel high
  // limit is met or exceeded.
  uint8_t buf;
  bool    i_high;
  bool    e_high;
  bool    vsrc_high;
  bool    vsense_high;
  m_i2c.read(m_addr, REG_HIGH_LIMIT_STATUS, &buf, 1);
  // The Internal Diode channel met or exceeded its programmed high limit
  i_high = (buf & 0b01);
  // The External Diode channel met or exceeded its programmed high limit
  e_high = (buf & 0b10);
  // the VSOURCE value met or exceeded its programmed high limit
  vsrc_high = buf & (1 << 6);
  // the VSENSE value met or exceeded its programmed high limit.
  vsense_high            = buf & (1 << 7);
  etl::vector<bool, 4> v = {i_high, e_high, vsrc_high, vsense_high};
  return v;
}

etl::vector<bool, 4>
emc1702::get_low_limit_status() const
{
  // Get the status bits that are set when a temperature or voltage channel low
  // limit is met or exceeded.
  uint8_t buf;
  bool    i_low;
  bool    e_low;
  bool    vsrc_low;
  bool    vsense_low;
  m_i2c.read(m_addr, REG_LOW_LIMIT_STATUS, &buf, 1);
  // The Internal Diode channel dropped below its programmed low limit
  i_low = (buf & 0b01);
  // The External Diode channel dropped below its programmed low limit
  e_low = (buf & 0b10);
  // the VSOURCE value dropped below its programmed low limit
  vsrc_low = buf & (1 << 6);
  // the VSENSE value dropped below its programmed low limit
  vsense_low             = buf & (1 << 7);
  etl::vector<bool, 4> v = {i_low, e_low, vsrc_low, vsense_low};
  return v;
}

bool
emc1702::get_crit_limit_status() const
{
  // Get the status bits that are set when a temperature or voltage channel
  // critical limit is met or exceeded.
  uint8_t buf;
  bool    i_crit;
  bool    e_crit;
  bool    vsrc_crit;
  bool    vsense_crit;
  m_i2c.read(m_addr, REG_CRIT_LIMIT_STATUS, &buf, 1);
  // The Internal Diode channel met or exceeded its programmed critical limit
  i_crit = (buf & 0b01);
  // The External Diode channel met or exceeded its programmed critical limit
  e_crit = (buf & 0b10);
  // the VSOURCE value met or exceeded its programmed critical limit
  vsrc_crit = buf & (1 << 6);
  // the VSENSE value met or exceeded its programmed critical limit.
  vsense_crit = buf & (1 << 7);
  // IF ANY OF THE PREVIOUS VALUES ARE TRU THE THERM PIN WILL BE ASERTED
  bool thermal_shutdown_needed = (i_crit || e_crit || vsrc_crit || vsense_crit);
  return thermal_shutdown_needed;
}

void
emc1702::set_averaging_control(averaging_control a) const
{
  uint8_t buf;
  // Set averaging control
  switch (a) {
  case averaging_control::DISABLED:
  case averaging_control::AVERAGING2X:
  case averaging_control::AVERAGING4X:
  case averaging_control::AVERAGING8X:
    buf = static_cast<uint8_t>(a);
    m_i2c.write(m_addr, REG_AVERAGING_CONTROL, &buf, 1);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }
  return;
}

bool
emc1702::get_sensor_info() const
{
  // Get sensor data
  uint8_t buf[4] = {0, 0, 0, 0};
  bool    ok     = false;
  // Get Product Features
  m_i2c.read(m_addr, REG_PRODUCT_FEATURES, buf, 4);
  if ((buf[1] == 0b00111001) && (buf[2] == 0b01011101) &&
      (buf[3] == 0b10000010)) {
    ok = true;
  }
  return ok;
}

void
emc1702::set_voltage_sampling_config(consecutive_alert_voltage v,
                                     averaging_control         s) const
{
  uint8_t buf;
  switch (v) {
  case consecutive_alert_voltage::OUT_OF_LIM_MEAS_1:
  case consecutive_alert_voltage::OUT_OF_LIM_MEAS_2:
  case consecutive_alert_voltage::OUT_OF_LIM_MEAS_3:
  case consecutive_alert_voltage::OUT_OF_LIM_MEAS_4:
    buf = static_cast<uint8_t>(v);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }

  switch (s) {
  case averaging_control::DISABLED:
  case averaging_control::AVERAGING2X:
  case averaging_control::AVERAGING4X:
  case averaging_control::AVERAGING8X:
    buf |= static_cast<uint8_t>(s);
    m_i2c.write(m_addr, VOLTAGE_SAMPLING_CONFIG, &buf, 1);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }
  return;
}

void
emc1702::set_current_sense_sampling_config(consecutive_alert_current v,
                                           averaging_control         i,
                                           current_sampling_time     t,
                                           max_expected_voltage      m) const
{
  uint8_t buf;
  switch (v) {
  case consecutive_alert_current::OUT_OF_LIM_MEAS_1:
  case consecutive_alert_current::OUT_OF_LIM_MEAS_2:
  case consecutive_alert_current::OUT_OF_LIM_MEAS_3:
  case consecutive_alert_current::OUT_OF_LIM_MEAS_4:
    buf = static_cast<uint8_t>(v);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }
  switch (i) {
  case averaging_control::DISABLED:
  case averaging_control::AVERAGING2X:
  case averaging_control::AVERAGING4X:
  case averaging_control::AVERAGING8X:
    buf |= (static_cast<uint8_t>(i) << 4);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }
  switch (t) {
  case current_sampling_time::S_T_82_MS:
  case current_sampling_time::S_T_164_MS:
  case current_sampling_time::S_T_328_MS:
    buf |= static_cast<uint8_t>(t);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }
  switch (m) {
  case max_expected_voltage::CURRENT_SENSOR_RANGE_10_mV:
  case max_expected_voltage::CURRENT_SENSOR_RANGE_20_mV:
  case max_expected_voltage::CURRENT_SENSOR_RANGE_40_mV:
  case max_expected_voltage::CURRENT_SENSOR_RANGE_80_mV:
    buf |= static_cast<uint8_t>(m);
    m_i2c.write(m_addr, CURRENT_SENSE_SAMPLING_CONFIG, &buf, 1);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }
  return;
}

void
emc1702::set_peak_detection_config(peak_detection_threshold t,
                                   peak_detection_duration  d)
{
  uint8_t buf;
  switch (t) {
  case peak_detection_threshold::THRESHOLD_10_mV:
  case peak_detection_threshold::THRESHOLD_15_mV:
  case peak_detection_threshold::THRESHOLD_20_mV:
  case peak_detection_threshold::THRESHOLD_25_mV:
  case peak_detection_threshold::THRESHOLD_30_mV:
  case peak_detection_threshold::THRESHOLD_35_mV:
  case peak_detection_threshold::THRESHOLD_40_mV:
  case peak_detection_threshold::THRESHOLD_45_mV:
  case peak_detection_threshold::THRESHOLD_50_mV:
  case peak_detection_threshold::THRESHOLD_55_mV:
  case peak_detection_threshold::THRESHOLD_60_mV:
  case peak_detection_threshold::THRESHOLD_65_mV:
  case peak_detection_threshold::THRESHOLD_70_mV:
  case peak_detection_threshold::THRESHOLD_75_mV:
  case peak_detection_threshold::THRESHOLD_80_mV:
  case peak_detection_threshold::THRESHOLD_85_mV:
    buf = static_cast<uint8_t>(t);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }

  switch (d) {
  case peak_detection_duration::DURATION_1_00_ms:
  case peak_detection_duration::DURATION_5_12_ms:
  case peak_detection_duration::DURATION_25_60_ms:
  case peak_detection_duration::DURATION_51_20_ms:
  case peak_detection_duration::DURATION_76_80_ms:
  case peak_detection_duration::DURATION_102_40_ms:
  case peak_detection_duration::DURATION_128_00_ms:
  case peak_detection_duration::DURATION_256_00_ms:
  case peak_detection_duration::DURATION_384_00_ms:
  case peak_detection_duration::DURATION_512_00_ms:
  case peak_detection_duration::DURATION_768_00_ms:
  case peak_detection_duration::DURATION_1024_00_ms:
  case peak_detection_duration::DURATION_1536_00_ms:
  case peak_detection_duration::DURATION_2048_00_ms:
  case peak_detection_duration::DURATION_3072_00_ms:
  case peak_detection_duration::DURATION_4096_00_ms:
    buf |= static_cast<uint8_t>(d);
    m_i2c.write(m_addr, PEAK_DETECTION_CONFIG, &buf, 1);
    break;
  default:
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  }
  return;
}

float
emc1702::get_sense_current() const
{
  uint8_t buf1;
  bool    sign;
  float   fsr;
  m_i2c.read(m_addr, CURRENT_SENSE_SAMPLING_CONFIG, &buf1, 1);
  uint8_t              r     = (buf1 & 0b11);
  max_expected_voltage range = static_cast<max_expected_voltage>(r);
  switch (range) {
  case max_expected_voltage::CURRENT_SENSOR_RANGE_10_mV:
    fsr = 10;
    break;
  case max_expected_voltage::CURRENT_SENSOR_RANGE_20_mV:
    fsr = 20;
    break;
  case max_expected_voltage::CURRENT_SENSOR_RANGE_40_mV:
    fsr = 40;
    break;
  case max_expected_voltage::CURRENT_SENSOR_RANGE_80_mV:
    fsr = 80;
    break;
  default:
    fsr = 0;
  }

  uint8_t buf[2];
  m_i2c.read(m_addr, SENCE_VOLTAGE_HIGH_BYTE, buf, 2);
  int16_t bit_value = ((buf[0] << 8) | buf[1]) >> 4;
  sign              = (bit_value >> 11) != 0;
  if (sign) {
    bit_value = ~bit_value + 1;
  }
  float fsc           = fsr / 10; // for 10 mΩ resistance
  float sense_current = fsc * static_cast<float>(bit_value) / 4096.0f * 2;
  return sense_current;
  // L1 data (in A)
}

float
emc1702::get_source_voltage() const
{
  uint8_t buf[2];
  m_i2c.read(m_addr, SOURCE_VOLTAGE_HIGH_BYTE, buf, 2);
  int16_t bit_value      = ((buf[0] << 8) | buf[1]) >> 5;
  float   source_voltage = static_cast<float>(bit_value) / 85.47f;
  return source_voltage;
  // L1 data (in V)
}

float
emc1702::get_power() const
{
  uint8_t buf[2] = {0, 0};
  m_i2c.read(m_addr, POWER_RATIO_VOLTAGE_HIGH_BYTE, buf, 2);
  uint16_t bit_value   = ((buf[0] << 8) | buf[1]);
  float    power_ratio = static_cast<float>(bit_value);
  return power_ratio;
  // L1 data (Power ratio)
}

void
emc1702::set_v_sense_lim(float v_sense_lim_low, float v_sense_lim_high) const
{
  // Set Sense Voltage Limit Registers
  // Set low limit
  uint8_t buf = 0;
  if (v_sense_lim_low >= 2032.0f) {
    buf = 0b01111111;
  } else if (v_sense_lim_low <= -2033.0f) {
    buf = 0b10000000;
  } else {
    if (v_sense_lim_low >= 0) {
      buf =
          static_cast<uint8_t>(floor((v_sense_lim_low / 16.0f) + 0.5f) * 16.0f);
    } else {
      buf =
          static_cast<uint8_t>(floor((v_sense_lim_low / 16.0f) + 0.5f) * 16.0f);
      buf = ~buf;
      buf++;
    }
  }
  m_i2c.write(m_addr, SENCE_VOLTAGE_LOW_LIM, &buf, 1);
  // Set high limit
  if (v_sense_lim_high >= 2032.0f) {
    buf = 0b01111111;
  } else if (v_sense_lim_high <= -2033.0f) {
    buf = 0b10000000;
  } else {
    if (v_sense_lim_high >= 0.0f) {
      buf = static_cast<uint8_t>(floor((v_sense_lim_high / 16.0f) + 0.5f) *
                                 16.0f);
    } else {
      buf = static_cast<uint8_t>(floor((v_sense_lim_high / 16.0f) + 0.5f) *
                                 16.0f);
      buf = ~buf;
      buf++;
    }
  }
  m_i2c.write(m_addr, SENCE_VOLTAGE_HIGH_LIM, &buf, 1);
}

void
emc1702::set_v_source_lim(float v_source_lim_low, float v_source_lim_high) const
{
  uint8_t buf[2] = {0, 0};
  if (v_source_lim_low < 0) {
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  } else if (v_source_lim_low >= 23.9063f) {
    buf[1] = 0b11111111;
  } else {
    buf[1] = static_cast<uint8_t>(round(v_source_lim_low * 10.67f));
  }
  if (v_source_lim_high < 0) {
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  } else if (v_source_lim_high >= 23.9063f) {
    buf[0] = 0b11111111;
  } else {
    buf[0] = static_cast<uint8_t>(round(v_source_lim_high * 10.67f));
  }
  m_i2c.write(m_addr, SOURCE_VOLTAGE_HIGH_LIM, buf, 2);
}

void
emc1702::set_v_crit_lim(float v_sense_lim_crit, float v_source_lim_crit,
                        float v_sense_hysterisis,
                        float v_source_hysterisis) const
{
  // Set Critical Voltage Limit Registers
  uint8_t buf = 0;
  if (v_sense_lim_crit >= 2032.0f) {
    buf = 0b01111111;
  } else if (v_sense_lim_crit <= -2033.0f) {
    buf = 0b10000000;
  } else {
    if (v_sense_lim_crit >= 0.0f) {
      buf = static_cast<uint8_t>(floor((v_sense_lim_crit / 16.0f) + 0.5f) *
                                 16.0f);
    } else {
      buf = static_cast<uint8_t>(floor((v_sense_lim_crit / 16.0f) + 0.5f) *
                                 16.0f);
      buf = ~buf;
      buf++;
    }
  }
  m_i2c.write(m_addr, SENCE_VOLTAGE_V_CRIT, &buf, 1);

  if (v_sense_hysterisis >= 496.0f) {
    buf = 0b00011111;
  } else if (v_sense_hysterisis < 0.0f) {
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  } else {
    buf = static_cast<uint8_t>(floor((v_sense_hysterisis / 16.0f) + 0.5f) *
                               16.0f);
  }
  m_i2c.write(m_addr, SENSE_V_CRIT_HYSTERISIS, &buf, 1);

  if (v_source_lim_crit < 0.0f) {
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  } else if (v_source_lim_crit >= 23.9063f) {
    buf = 0b11111111;
  } else {
    buf = static_cast<uint8_t>(round(v_source_lim_crit * 10.67f));
  }
  m_i2c.write(m_addr, SOURCE_VOLTAGE_V_CRIT, &buf, 1);
  if (v_source_hysterisis < 0.0f) {
    SATNOGS_COMMS_ERROR(emc1702_inval_param_exception);
  } else if (v_source_hysterisis >= 2.9063f) {
    buf = 0b00011111;
  } else {
    buf = static_cast<uint8_t>(round(v_source_hysterisis * 10.67f));
  }
  m_i2c.write(m_addr, SOURCE_V_CRIT_HYSTERISIS, &buf, 1);
}

bool
emc1702::alert() const
{
  return m_alert.get();
}

} // namespace satnogs::comms
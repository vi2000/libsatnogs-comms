/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <algorithm>
#include <satnogs-comms/tmp117.hpp>

namespace satnogs::comms
{

#define REG_TEMPERATURE        0x00
#define REG_CONFIG             0x01
#define REG_HI_LIMIT           0x02
#define REG_LO_LIMIT           0x03
#define REG_EEPROM_UNLOCK      0x04
#define REG_EEPROM1            0x05
#define REG_EEPROM2            0x06
#define REG_EEPROM3            0x08
#define REG_TEMPERATURE_OFFSET 0x07
#define REG_ID                 0x0F

tmp117::tmp117(const etl::istring &name, bsp::i2c &i2c, uint16_t addr)
    : tmp117(name, i2c, addr, m_dummy)
{
}

tmp117::tmp117(const etl::istring &name, bsp::i2c &i2c, uint16_t addr,
               bsp::gpio &alert)
    : m_name(name),
      m_i2c(i2c),
      m_addr(addr),
      m_alert(alert),
      c(),
      mode(sensor_mode::CONTINUOUS),
      conv(conversion_time::CC_15_MS_5),
      avrg(averaging_mode::AVE8),
      pin_mod(pin_mode::ALERT),
      pin_pol(pin_polarity::ACTIVE_LOW),
      a_p_s(alert_pin_select::ALERT_FLAG)
{
  set_config();
  set_temp_lim();
  set_offset_temp();
}

void
tmp117::set_config() const
{
  uint8_t buf[2] = {0, 0};
  set_sensor_mode(mode, buf[1], buf[0]);
  set_conv_time(conv, buf[1], buf[0]);
  set_averaging(avrg, buf[1], buf[0]);
  set_pin_mode(pin_mod, buf[1], buf[0]);
  set_pin_polarity(pin_pol, buf[1], buf[0]);
  set_alert_pin_select(a_p_s, buf[1], buf[0]);
  m_i2c.write(m_addr, REG_CONFIG, buf, 2);
  return;
}

float
tmp117::temperature() const
{
  uint8_t buf[2] = {0, 0};
  m_i2c.read(m_addr, REG_TEMPERATURE, buf, 2);
  return (static_cast<int16_t>((buf[0] << 8) | buf[1])) *
         tmp117_resolution; // °C
}

bool
tmp117::alert() const
{
  return m_alert.get();
}

void
tmp117::get_status(config &c) const
{
  c = {.high_alert    = 0,
       .low_alert     = 0,
       .data_ready    = 0,
       .eeprom_busy   = 0,
       .mod           = 0b00,
       .conv          = 0b000,
       .avg           = 0b00,
       .term_or_alert = 0,
       .pol           = 0,
       .dr_or_alert   = 0,
       .soft_reset    = 0};

  uint8_t buf[2] = {0, 0};
  m_i2c.read(m_addr, REG_CONFIG, buf, 2);
  c.high_alert    = buf[0] & (1 << 7);
  c.low_alert     = buf[0] & (1 << 6);
  c.data_ready    = buf[0] & (1 << 5);
  c.eeprom_busy   = buf[0] & (1 << 4);
  c.mod           = (buf[0] >> 2) & 0b11;
  c.conv          = ((buf[0] & 0b11) << 1) | (buf[1] & (1 << 7));
  c.avg           = (buf[1] >> 5) & 0b11;
  c.term_or_alert = buf[1] & (1 << 4);
  c.pol           = buf[1] & (1 << 3);
  c.dr_or_alert   = buf[1] & (1 << 2);
  c.soft_reset    = buf[1] & 0b10;
  return;
}

void
tmp117::set_temp_lim(float low_lim, float high_lim) const
{
  uint8_t buf[2] = {0, 0};
  // Set Low Limit Temperature
  int16_t low_lim_buf = static_cast<int16_t>(
      std::clamp(low_lim, -256.0f, 256.0f - tmp117_resolution) /
      tmp117_resolution);
  buf[0] = static_cast<uint8_t>(low_lim_buf >> 8);
  buf[1] = static_cast<uint8_t>(low_lim_buf);
  m_i2c.write(m_addr, REG_LO_LIMIT, buf, 2);
  // Set High Limit Temperature
  int16_t high_lim_buf =
      static_cast<int16_t>(std::min(high_lim, 256.0f) - tmp117_resolution) /
      tmp117_resolution;
  buf[0] = static_cast<uint8_t>(high_lim_buf >> 8);
  buf[1] = static_cast<uint8_t>(high_lim_buf);
  m_i2c.write(m_addr, REG_HI_LIMIT, buf, 2);
}

bool
tmp117::get_eeprom_busy() const
{
  uint8_t buf = 0;
  m_i2c.read(m_addr, REG_EEPROM_UNLOCK, &buf, 1);
  return (bool)(buf & (1 << 6));
}

void
tmp117::unlock_eeprom() const
{
  uint8_t buf = 0x80;
  m_i2c.write(m_addr, REG_EEPROM_UNLOCK, &buf, 1);
}

void
tmp117::lock_eeprom() const
{
  uint8_t buf = 0x00;
  m_i2c.write(m_addr, REG_EEPROM_UNLOCK, &buf, 1);
}

uint16_t
tmp117::read_eeprom(uint8_t eeprom_number) const
{
  uint8_t buf[2] = {0, 0};
  if (!get_eeprom_busy()) {
    switch (eeprom_number) {
    case 1:
      m_i2c.read(m_addr, REG_EEPROM1, buf, 2);
      break;
    case 2:
      m_i2c.read(m_addr, REG_EEPROM2, buf, 2);
      break;
    case 3:
      m_i2c.read(m_addr, REG_EEPROM3, buf, 2);
      break;
    default:
      SATNOGS_COMMS_ERROR(tmp117_inval_param_exception);
    }
  } else {
    SATNOGS_COMMS_ERROR(tmp117_not_ready);
  }
  return static_cast<uint16_t>((buf[0] << 8) | buf[1]);
}

void
tmp117::write_eeprom(uint8_t eeprom_number, uint16_t val) const
{
  uint8_t buf[2] = {0, 0};
  buf[0]         = static_cast<uint8_t>(val >> 8);
  buf[1]         = static_cast<uint8_t>(val);
  if (!get_eeprom_busy()) {
    switch (eeprom_number) {
    case 1:
      m_i2c.write(m_addr, REG_EEPROM1, buf, 2);
      break;
    case 2:
      m_i2c.write(m_addr, REG_EEPROM2, buf, 2);
      break;
    case 3:
      m_i2c.write(m_addr, REG_EEPROM3, buf, 2);
      break;
    default:
      SATNOGS_COMMS_ERROR(tmp117_inval_param_exception);
    }
  } else {
    SATNOGS_COMMS_ERROR(tmp117_not_ready);
  }
}

void
tmp117::set_offset_temp(float offset) const
{
  uint8_t buf[2] = {0, 0};
  // Set Low Limit Temperature
  int16_t offset_buf = static_cast<int16_t>(
      std::clamp(offset, -256.0f, 256.0f - tmp117_resolution) /
      tmp117_resolution);
  buf[0] = static_cast<uint8_t>(offset_buf >> 8);
  buf[1] = static_cast<uint8_t>(offset_buf);
  m_i2c.write(m_addr, REG_TEMPERATURE_OFFSET, buf, 2);
}

float
tmp117::get_offset_temp() const
{
  uint8_t buf[2] = {0, 0};
  m_i2c.read(m_addr, REG_TEMPERATURE_OFFSET, buf, 2);
  return (static_cast<int16_t>((buf[0] << 8) | buf[1])) *
         tmp117_resolution; // °C
}

uint16_t
tmp117::get_device_id() const
{
  uint8_t buf[2] = {0, 0};
  m_i2c.read(m_addr, REG_ID, buf, 2);
  uint16_t bit_value = (buf[0] << 8) | buf[1];
  return (bit_value & 0x0fff);
}

uint8_t
tmp117::get_device_rev() const
{
  uint8_t buf = 0;
  m_i2c.read(m_addr, REG_ID, &buf, 1);
  return (buf >> 4);
}

void
tmp117::set_sensor_mode(sensor_mode m, uint8_t &high_byte, uint8_t &low_byte)
{
  uint8_t mask = 0b00110000;
  switch (m) {
  case sensor_mode::CONTINUOUS:
  case sensor_mode::SHUTDOWN:
  case sensor_mode::ONESHOT:
    low_byte = (low_byte & ~mask) | (static_cast<uint8_t>(m) << 4 & mask);
    break;
  default:
    SATNOGS_COMMS_ERROR(tmp117_inval_param_exception);
  }
}

void
tmp117::set_conv_time(conversion_time t, uint8_t &high_byte, uint8_t &low_byte)
{
  uint8_t mask = 0b00000011;
  switch (t) {
  case conversion_time::CC_15_MS_5:
  case conversion_time::CC_125_MS:
  case conversion_time::CC_250_MS:
  case conversion_time::CC_500_MS:
  case conversion_time::CC_1_S:
  case conversion_time::CC_4_S:
  case conversion_time::CC_8_S:
  case conversion_time::CC_16_S:
    low_byte = (low_byte & ~mask) | ((static_cast<uint8_t>(t) >> 1) & mask);
    high_byte =
        (high_byte & ~(1 << 7)) | ((static_cast<uint8_t>(t) << 7) & (1 << 7));
    break;
  default:
    SATNOGS_COMMS_ERROR(tmp117_inval_param_exception);
  }
}

void
tmp117::set_averaging(averaging_mode a, uint8_t &high_byte, uint8_t &low_byte)
{
  uint8_t mask = 0b01100000;
  switch (a) {
  case averaging_mode::NO_AVE:
  case averaging_mode::AVE8:
  case averaging_mode::AVE32:
  case averaging_mode::AVE64:
    high_byte = (high_byte & ~mask) | ((static_cast<uint8_t>(a) << 5) & mask);
    break;
  default:
    SATNOGS_COMMS_ERROR(tmp117_inval_param_exception);
  }
}

void
tmp117::set_pin_mode(pin_mode p_m, uint8_t &high_byte, uint8_t &low_byte)
{
  uint8_t mask = 0b00010000;
  switch (p_m) {
  case pin_mode::ALERT:
  case pin_mode::THERMAL:
    high_byte = (high_byte & ~mask) | ((static_cast<uint8_t>(p_m) << 4) & mask);
    break;
  default:
    SATNOGS_COMMS_ERROR(tmp117_inval_param_exception);
  }
}

void
tmp117::set_pin_polarity(pin_polarity p_p, uint8_t &high_byte,
                         uint8_t &low_byte)
{
  uint8_t mask = 0b00001000;
  switch (p_p) {
  case pin_polarity::ACTIVE_LOW:
  case pin_polarity::ACTIVE_HIGH:
    high_byte = (high_byte & ~mask) | ((static_cast<uint8_t>(p_p) << 3) & mask);
    break;
  default:
    SATNOGS_COMMS_ERROR(tmp117_inval_param_exception);
  }
}

void
tmp117::set_alert_pin_select(alert_pin_select a_p, uint8_t &high_byte,
                             uint8_t &low_byte)
{
  uint8_t mask = 0b00000100;
  switch (a_p) {
  case alert_pin_select::ALERT_FLAG:
  case alert_pin_select::DATA_READY_FLAG:
    high_byte = (high_byte & ~mask) | ((static_cast<uint8_t>(a_p) << 2) & mask);
    break;
  default:
    SATNOGS_COMMS_ERROR(tmp117_inval_param_exception);
  }
}

} // namespace satnogs::comms

/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/error.hpp>
#include <satnogs-comms/exception.hpp>
#include <satnogs-comms/fpga.hpp>
#include <satnogs-comms/power.hpp>

namespace satnogs::comms
{

/**
 * @brief Construct a new fpga::fpga object and disables it to ensure now excess
 * power is accidentally used during resets
 *
 * @param h the FPGA type attached
 * @param spi SPI peripheral to communicate with the FPGA
 * @param pwr the power subsystem
 */
fpga::fpga(hw h, bsp::spi &spi, power &pwr) : m_hw(h), m_spi(spi), m_pwr(pwr)
{
  switch (m_hw) {
  case hw::NONE:
    m_name = etl::make_string("N/A");
    break;
  case hw::ALINX_AC7Z020:
    m_name = etl::make_string("ALINX-AC7Z020");
    break;
  case hw::LIBREWAVE:
    m_name = etl::make_string("LIBREWAVE");
    break;
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
  enable(false);
}

/**
 * @brief Enable/disable the FPGA subsystem
 *
 * @param en true to enable, false to disable
 */
void
fpga::enable(bool en)
{
  m_pwr.enable(power::subsys::FPGA_5V, en);
}

/**
 * @brief Returns the state of the FPGA
 *
 * @return true if it is enabled
 * @return false if it is disabled
 */
bool
fpga::enabled() const
{
  return m_pwr.enabled(power::subsys::FPGA_5V);
}

void
fpga::write_reg(uint32_t reg, uint32_t val)
{
  // TODO
}

uint32_t
fpga::read_reg(uint32_t reg)
{
  // TODO
  return 0;
}

void
fpga::write_reg(reg r, uint32_t val)
{
  // TODO
}

uint32_t
fpga::read_reg(reg r)
{
  // TODO
  return 0;
}

} // namespace satnogs::comms

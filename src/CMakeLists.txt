# ##############################################################################
# SatNOGS-COMMS control library
#
# Copyright (C) 2021-2022, Libre Space Foundation <http://libre.space>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GNU General Public License v3.0 or later
# ##############################################################################

add_subdirectory(drivers)
add_library(libsatnogs-comms INTERFACE)

target_sources(
  libsatnogs-comms
  INTERFACE # cmake-format: sortable
            ad8318.cpp
            board.cpp
            emc1702.cpp
            emmc.cpp
            f2972.cpp
            fpf270x.cpp
            fpga.cpp
            ina322x.cpp
            leds.cpp
            lna.cpp
            lp5912.cpp
            power.cpp
            radio.cpp
            rf_frontend.cpp
            rf_frontend09.cpp
            rf_frontend24.cpp
            tmp117.cpp)

target_include_directories(
  libsatnogs-comms
  INTERFACE ${CMAKE_CURRENT_LIST_DIR}/../include
            ${CMAKE_CURRENT_LIST_DIR}/../etl/include
            ${CMAKE_CURRENT_BINARY_DIR}/../include ${CMAKE_CURRENT_LIST_DIR})
target_link_libraries(libsatnogs-comms INTERFACE libsatnogs-comms-drivers)

if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
  # CMAKE_CXX_STANDARD for MSVC should be fixed in CMake 3.10.
  target_compile_options(${PROJECT_NAME} PRIVATE /std:c++latest)
endif()

/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2021-2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/error.hpp>
#include <satnogs-comms/exception.hpp>
#include <satnogs-comms/fpf270x.hpp>

namespace satnogs::comms
{

fpf270x::fpf270x(const etl::istring &name, bsp::gpio &on, bsp::gpio &pgood,
                 bsp::gpio &flagb)
    : m_name(name), m_on_gpio(on), m_pgood_gpio(pgood), m_flagb_gpio(flagb)
{
  enable(false);
}

const etl::istring &
fpf270x::name() const
{
  return m_name;
}

/**
 * @brief Set the logical level of the EN pin. Note that this is nornally active
 * low!
 *
 * @param set the logical level of the EN pin
 */
void
fpf270x::enable(bool set)
{
  m_on_gpio.set(set);
}

bool
fpf270x::enabled() const
{
  return m_on_gpio.get();
}

bool
fpf270x::pgood() const
{
  return m_pgood_gpio.get();
}

bool
fpf270x::flagb() const
{
  return m_flagb_gpio.get();
}

} // namespace satnogs::comms

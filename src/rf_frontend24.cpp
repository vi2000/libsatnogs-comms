/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/bsp/chrono.hpp>
#include <satnogs-comms/error.hpp>
#include <satnogs-comms/rf_frontend24.hpp>

namespace satnogs::comms
{

/* Re-implementation of the RFFC2071 driver weak functions */
extern "C" {

int
rffcx07x_set_enx(struct rffcx07x *h, uint8_t enable)
{
  if (h->enx_gpio_dev) {
    bsp::gpio *enx_gpio_dev = static_cast<bsp::gpio *>(h->enx_gpio_dev);
    enx_gpio_dev->set(enable);
  }
  return RFFCX07X_NO_ERROR;
}

int
rffcx07x_set_enbl(struct rffcx07x *h, uint8_t enable)
{
  return RFFCX07X_NO_ERROR;
}

int
rffcx07x_set_resetx(struct rffcx07x *h, uint8_t enable)
{
  if (h->rst_gpio_dev) {
    bsp::gpio *rst_gpio_dev = static_cast<bsp::gpio *>(h->rst_gpio_dev);
    rst_gpio_dev->set(enable);
  }
  return RFFCX07X_NO_ERROR;
}

int
rffcx07x_set_mode(struct rffcx07x *h, uint8_t enable)
{
  return RFFCX07X_NO_ERROR;
}

int
rffcx07x_set_clk(struct rffcx07x *h, uint8_t enable)
{
  if (h->clk_dev) {
    bsp::gpio *clk_dev = static_cast<bsp::gpio *>(h->clk_dev);
    clk_dev->set(enable);
  }
  return RFFCX07X_NO_ERROR;
}

int
rffcx07x_set_sda(struct rffcx07x *h, uint8_t enable)
{
  if (h->sda_gpio_dev) {
    bsp::gpio *sda_gpio_dev = static_cast<bsp::gpio *>(h->sda_gpio_dev);
    sda_gpio_dev->set(enable);
  }
  return RFFCX07X_NO_ERROR;
}

int
rffcx07x_get_sda(struct rffcx07x *h)
{
  if (h->sda_gpio_dev) {
    bsp::gpio *sda_gpio_dev = static_cast<bsp::gpio *>(h->sda_gpio_dev);
    return sda_gpio_dev->get();
  }
  return -RFFCX07X_NOT_IMPL;
}

int
rffcx07x_set_sda_dir(struct rffcx07x *h, rffcx07x_sda_dir_t dir)
{
  if (h->sda_gpio_dev) {
    bsp::gpio *sda_gpio_dev = static_cast<bsp::gpio *>(h->sda_gpio_dev);
    if (dir == RFFCX07X_SDA_RX) {
      sda_gpio_dev->set_direction(bsp::gpio::direction::INPUT);
    } else {
      sda_gpio_dev->set_direction(bsp::gpio::direction::OUTPUT);
    }
  }
  return RFFCX07X_NO_ERROR;
}

void
rffcx07x_en_irq(struct rffcx07x *h, uint8_t enable)
{
  return;
}

void
rffcx07x_delay_us(struct rffcx07x *h, uint32_t us)
{
  bsp::chrono *chrono_dev = static_cast<bsp::chrono *>(h->user_dev0);
  chrono_dev->delay_us(us);
}
}

rf_frontend24::rf_frontend24(io_conf &io, power &pwr)
    : rf_frontend({2.1e9, 2.5e9}, {2.1e9, 2.5e9},
                  {io.en_agc, io.agc_vset, io.flt_sel}, pwr),
      m_chrono(io.chrono)
{
  m_mixer.clk_dev      = &io.mixer_clk;
  m_mixer.rst_gpio_dev = &io.mixer_rst;
  m_mixer.enx_gpio_dev = &io.mixer_enx;
  m_mixer.sda_gpio_dev = &io.mixer_sda;
  m_mixer.user_dev0    = &io.chrono;
  enable(false);
}

/**
 * Enables the basic subsystems of the RF frontend. Subsystems that are
 * used for a specific direction (RX or TX) are explicitly disabled to reduce
 * power consumption.
 *
 * Users may have to call the set_direction() method to enable the corresponding
 * subsystems such as PA or LNA.
 *
 * @param set true to enable, false to disable.
 */
void
rf_frontend24::enable(bool set)
{
  int ret = 0;
  m_pwr.enable(power::subsys::SBAND, set);
  if (set) {
    m_chrono.delay_us(2000);
    ret = rffcx07x_init(&m_mixer, 26000000, 72800, RFFCX07X_HD);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);

    ret = rffcx07x_set_gate(&m_mixer, 1);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 1, 1); // Narrow filter
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 1, 2); // NC
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 1, 3); // RX disabled
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 0, 4); // AGC disabled
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 1, 5); // HPA disabled
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception)
    ret = rffcx07x_set_gpo(&m_mixer, 0, 6); // RX amplifiers disabled
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
  }
}

bool
rf_frontend24::enabled() const
{
  return m_pwr.enabled(power::subsys::SBAND);
}

/**
 * Sets the direction of the RF frontend, enables the corresponding components
 * and disables the components that are not needed to reduce consumption
 * @param d the direction
 */
void
rf_frontend24::set_direction(dir d)
{
  int ret = 0;
  rf_frontend::set_direction(d);
  switch (d) {
  case dir::RX:
    ret = rffcx07x_set_mix_current(&m_mixer, 5, RFFCX07X_PATH2);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_enable(&m_mixer, 0, RFFCX07X_PATH2, 1);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_enable(&m_mixer, 1, RFFCX07X_PATH1, 1);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 1, 1); // Narrow filter
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 1, 2); // NC
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 0, 3); // RX enabled
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 1, 4); // AGC enabled
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 1, 5); // HPA disabled
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception)
    ret = rffcx07x_set_gpo(&m_mixer, 1, 6); // RX amplifiers enabled
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_freq(&m_mixer, mixing_freq, RFFCX07X_PATH1);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);

    if (mixer_lock() == false) {
      SATNOGS_COMMS_ERROR(mixer_lock_exception);
    }
    break;
  case dir::TX:
    ret = rffcx07x_set_mix_current(&m_mixer, 5, RFFCX07X_PATH2);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_enable(&m_mixer, 0, RFFCX07X_PATH1, 1);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_enable(&m_mixer, 1, RFFCX07X_PATH2, 1);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 1, 1); // Narrow filter
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 1, 2); // NC
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 1, 3); // RX disabled
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 0, 4); // AGC disabled
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_gpo(&m_mixer, 0, 5); // HPA enabled
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception)
    ret = rffcx07x_set_gpo(&m_mixer, 0, 6); // RX amplifiers disabled
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);
    ret = rffcx07x_set_freq(&m_mixer, mixing_freq, RFFCX07X_PATH2);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, rf_frontend24_exception);

    if (mixer_lock() == false) {
      SATNOGS_COMMS_ERROR(mixer_lock_exception);
    }
    break;
  default:
    SATNOGS_COMMS_ERROR(rf_frontend_inval_param_exception);
    break;
  }
}

bool
rf_frontend24::mixer_lock()
{
  uint8_t lock = false;
  int     ret  = rffcx07x_get_lock(&m_mixer, &lock);
  return lock && ret == RFFCX07X_NO_ERROR;
}

} // namespace satnogs::comms

/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/emmc.hpp>
#include <satnogs-comms/error.hpp>
#include <satnogs-comms/exception.hpp>

namespace satnogs::comms
{

/**
 * Creates a generic EMMC handler
 * @param name the name of the EMMC
 * @param total_size the total size in bytes
 * @param start the start of the address space that can the MCU use
 * @param start the stop of the address space that can the MCU use
 * @param en GPIO to enable/disable the EMMC
 * @param sel GPIO to select which of MCU or FPGA has access to the EMMC
 */
emmc::emmc(const etl::istring &name, uint64_t total_size, uint64_t start,
           uint64_t stop, bsp::gpio &en, bsp::gpio &sel)
    : m_capasity(total_size),
      m_start(start),
      m_stop(stop),
      m_name(name),
      m_en_gpio(en),
      m_sel_gpio(sel)
{
  enable(false);
}

const etl::istring &
emmc::name() const
{
  return m_name;
}

/**
 * Enable/disable the EMMC
 * @param set set to true to enable the EMMC, false to disable.
 * If the logic is inverted, it is handled automatically internally
 */
void
emmc::enable(bool set)
{
  m_en_gpio.set(set);
}

/**
 *
 * @return true of the EMMC is enabled, false otherwise
 */
bool
emmc::enabled() const
{
  return m_en_gpio.get();
}

/**
 * Sets the direction of the EMMC
 * @param d the direction
 */
void
emmc::set_dir(dir d)
{
  switch (d) {
  case dir::MCU:
  case dir::FPGA:
    m_en_gpio.set(static_cast<bool>(d));
    break;
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
    break;
  }
}

/**
 *
 * @return the direction of the EMMC
 */
emmc::dir
emmc::get_dir() const
{
  if (m_en_gpio.get() == false) {
    return dir::MCU;
  }
  return dir::FPGA;
}

uint64_t
emmc::capacity() const
{
  return m_capasity;
}
uint64_t
emmc::start() const
{
  return m_start;
}
uint64_t
emmc::stop() const
{
  return m_stop;
}

} // namespace satnogs::comms

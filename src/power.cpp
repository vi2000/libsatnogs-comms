/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/power.hpp>

namespace satnogs::comms
{

/**
 * @brief Enable/disable the power of subsystems
 *
 * @param sys the target subsystem/power supply
 * @param en true to enable, false to disable
 */
void
power_base::enable(subsys sys, bool en)
{
  switch (sys) {
  case subsys::CAN1:
    m_can1_en.set(en);
    m_can1_low_pwr.set(!en);
    break;
  case subsys::CAN2:
    m_can2_en.set(en);
    m_can2_low_pwr.set(!en);
    break;
  case subsys::RF_5V:
    m_rf_5v_en.set(en);
    break;
  case subsys::FPGA_5V:
    m_fpga_5v_en.set(en);
    break;
  case subsys::CAN1_LPWR:
    m_can1_low_pwr.set(en);
    break;
  case subsys::CAN2_LPWR:
    m_can2_low_pwr.set(en);
    break;
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
}

/**
 * @brief Enable/disable the power of subsystems
 *
 * @param sys the target subsystem/power supply
 * @param en true to enable, false to disable
 */
void
power_v0_3::enable(subsys sys, bool en)
{
  switch (sys) {
  case subsys::CAN1:
    m_can1_en.set(en);
    m_can1_low_pwr.set(!en);
    break;
  case subsys::CAN2:
    m_can2_en.set(en);
    m_can2_low_pwr.set(!en);
    break;
  case subsys::RF_5V:
    m_rf_5v_en.set(en);
    break;
  case subsys::FPGA_5V:
    m_fpga_5v_en.set(en);
    break;
  case subsys::CAN1_LPWR:
    m_can1_low_pwr.set(en);
    break;
  case subsys::CAN2_LPWR:
    m_can2_low_pwr.set(en);
    break;
  case subsys::UHF:
    m_uhf_en.set(en);
    break;
  case subsys::SBAND:
    m_sband_en.set(en);
    break;
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
}

/**
 * @brief Checks the if the subsystem is enabled
 *
 * @param sys the subsystem to check
 * @return true if the subsystem is enabled, false otherwise
 */
bool
power_base::enabled(subsys sys) const
{
  switch (sys) {
  case subsys::CAN1:
    return m_can1_en.get();
  case subsys::CAN2:
    return m_can2_en.get();
  case subsys::RF_5V:
    return m_rf_5v_en.get();
  case subsys::FPGA_5V:
    return m_fpga_5v_en.get();
  case subsys::CAN1_LPWR:
    return m_can1_low_pwr.get();
  case subsys::CAN2_LPWR:
    return m_can2_low_pwr.get();
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
}

/**
 * @brief Checks the if the subsystem is enabled
 *
 * @param sys the subsystem to check
 * @return true if the subsystem is enabled, false otherwise
 */
bool
power_v0_3::enabled(subsys sys) const
{
  switch (sys) {
  case subsys::CAN1:
    return m_can1_en.get();
  case subsys::CAN2:
    return m_can2_en.get();
  case subsys::RF_5V:
    return m_rf_5v_en.get();
  case subsys::FPGA_5V:
    return m_fpga_5v_en.get();
  case subsys::CAN1_LPWR:
    return m_can1_low_pwr.get();
  case subsys::CAN2_LPWR:
    return m_can2_low_pwr.get();
  case subsys::UHF:
    return m_uhf_en.get();
  case subsys::SBAND:
    return m_sband_en.get();
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
}

/**
 * @brief Gets the power good indication from various power supplies
 *
 * @param tp the power supply
 * @return true if the power is good and stabilized, false otherwise
 */
bool
power_v0_2::pgood(pgood_tp tp) const
{
  switch (tp) {
  case pgood_tp::RAIL_5V:
    return m_pgood_5v.get();
  case pgood_tp::RAIL_3V3:
    return m_pgood_3v3.get();
  case pgood_tp::RAIL_FPGA:
    return m_pgood_fpga.get();
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
}

float
power_v0_2::voltage(channel sys) const
{
  switch (sys) {
  case channel::FPGA:
    return m_monitor.voltage(ina322x::channel::CH1);
  case channel::RF_5V_U:
    return m_monitor.voltage(ina322x::channel::CH2);
  case channel::RF_5V_S:
    return m_monitor.voltage(ina322x::channel::CH3);
  case channel::VIN:
    // v0.2 has no VIN measurement
    return std::numeric_limits<float>::signaling_NaN();
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
}

float
power_v0_2::current(channel sys) const
{
  switch (sys) {
  case channel::FPGA:
    return m_monitor.current(ina322x::channel::CH1);
  case channel::RF_5V_U:
    return m_monitor.current(ina322x::channel::CH2);
  case channel::RF_5V_S:
    return m_monitor.current(ina322x::channel::CH3);
  case channel::VIN:
    // v0.2 has no VIN measurement
    return std::numeric_limits<float>::signaling_NaN();
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
}

/**
 * @brief Gets the power good indication from various power supplies
 *
 * @param tp the power supply
 * @return true if the power is good and stabilized, false otherwise
 */
bool
power_v0_3::pgood(pgood_tp tp) const
{
  switch (tp) {
  /* In v0.3 if the 3V3 has fault, the MCU will reset */
  case pgood_tp::RAIL_5V:
  case pgood_tp::RAIL_3V3:
    return m_pgood_5v.get();
  case pgood_tp::RAIL_FPGA:
    return m_pgood_fpga.get();
  case pgood_tp::RAIL_UHF:
    return m_uhf_pgood.get();
  case pgood_tp::RAIL_SBAND:
    return m_sband_pgood.get();
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
}

float
power_v0_3::voltage(channel sys) const
{
  switch (sys) {
  case channel::FPGA:
  case channel::RF_5V_U:
  case channel::RF_5V_S:
    return std::numeric_limits<float>::signaling_NaN();
  case channel::VIN:
    return m_monitor.get_source_voltage();
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
}

void
power_v0_3::set_vref(channel sys) const
{
  switch (sys) {
  case channel::D_3V3:
    m_imon_3v3.set_vref(m_i_lim.d_3v3);
    break;
  case channel::RF_5V:
    m_imon_5v.set_vref(m_i_lim.rf_5v);
    break;
  case channel::FPGA:
    m_imon_fpga.set_vref(m_i_lim.fpga_5v);
    break;
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
}

float
power_v0_3::current(channel sys) const
{
  switch (sys) {
  case channel::D_3V3:
    return m_imon_3v3.read();
  case channel::RF_5V:
    return m_imon_5v.read();
  case channel::FPGA:
    return m_imon_fpga.read();
  case channel::EMC1702:
    return m_monitor.get_sense_current();
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
}

power_base::power_base(const base_io_conf &io)
    : m_rf_5v_en(io.rf_5v_en),
      m_fpga_5v_en(io.fpga_5v_en),
      m_can1_en(io.can1_en),
      m_can1_low_pwr(io.can1_low_pwr),
      m_can2_en(io.can2_en),
      m_can2_low_pwr(io.can2_low_pwr),
      m_pgood_5v(io.pgood_5v),
      m_pgood_fpga(io.pgood_5v_fpga)
{
  /* Disable all at startup */
  enable(subsys::CAN1, false);
  enable(subsys::CAN2, false);
  enable(subsys::RF_5V, false);
  enable(subsys::FPGA_5V, false);
}

power_v0_2::power_v0_2(io_conf &io)
    // DANGEROUS^{TM}! Unfortunately named initialization list is available from
    // C++20
    : power_base({io.rf_5v_en, io.fpga_5v_en, io.can1_en, io.can1_low_pwr,
                  io.can2_en, io.can2_low_pwr, io.rf_5v_pgood,
                  io.fpga_5v_pgood}),
      m_monitor(io.mon_i2c, 0x40),
      m_pgood_3v3(io.rf_3v3_pgood)
{
}

power_v0_3::power_v0_3(io_conf &io)
    // DANGEROUS^{TM}! Unfortunately named initialization list is available from
    // C++20
    : power_base({io.rf_5v_en, io.fpga_5v_en, io.can1_en, io.can1_low_pwr,
                  io.can2_en, io.can2_low_pwr, io.rf_5v_pgood,
                  io.fpga_5v_pgood}),
      m_monitor(etl::make_string("current-sensor"), io.mon_i2c, 0b101101),
      m_uhf_en(io.uhf_en),
      m_uhf_pgood(io.uhf_pgood),
      m_sband_en(io.sband_en),
      m_sband_pgood(io.sband_pgood),
      m_imon_5v(io.imon_5v_rf),
      m_imon_3v3(io.imon_3v3_d),
      m_imon_fpga(io.imon_fpga),
      m_i_lim(io.ilim)

{
  enable(subsys::UHF, false);
  enable(subsys::SBAND, false);
  m_imon_5v.start();
  m_imon_3v3.start();
  m_imon_fpga.start();
  set_vref(channel::D_3V3);
  set_vref(channel::RF_5V);
  set_vref(channel::FPGA);
}
power::power(io_conf &io) : power_driver(io) {}
} // namespace satnogs::comms

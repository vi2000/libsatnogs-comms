/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <etl/binary.h>
#include <etl/crc.h>
#include <etl/vector.h>
#include <satnogs-comms/board.hpp>
#include <satnogs-comms/bsp/chrono.hpp>
#include <satnogs-comms/dsp/ccsds.hpp>
#include <satnogs-comms/error.hpp>
#include <satnogs-comms/power.hpp>
#include <satnogs-comms/radio.hpp>

namespace satnogs::comms
{

static radio::rx_msg rx_msg;

/* The C++ way. This is the way! */
template <typename T, const size_t IDX,
          std::enable_if_t<std::is_integral_v<T>, bool> = true>
static constexpr T
bit()
{
  static_assert(IDX < sizeof(T) * 8, "The operation will overflow!");
  return static_cast<T>(1) << IDX;
}

/* Re-implementation of the AT86RF215 weak functions */
extern "C" {
int
at86rf215_set_rstn(struct at86rf215 *h, uint8_t enable)
{
  if (h->rst_gpio_dev) {
    bsp::gpio *rst_gpio_dev = static_cast<bsp::gpio *>(h->rst_gpio_dev);
    rst_gpio_dev->set(enable);
  }
  return AT86RF215_OK;
}

int
at86rf215_set_seln(struct at86rf215 *h, uint8_t enable)
{
  if (h->cs_gpio_dev) {
    bsp::gpio *cs_gpio_dev = static_cast<bsp::gpio *>(h->cs_gpio_dev);
    cs_gpio_dev->set(enable);
  }
  return AT86RF215_OK;
}

void
at86rf215_delay_us(struct at86rf215 *h, uint32_t us)
{
  bsp::chrono *chrono_dev = static_cast<bsp::chrono *>(h->user_dev0);
  chrono_dev->delay_us(us);
}

size_t
at86rf215_get_time_ms(struct at86rf215 *h)
{
  bsp::chrono *chrono_dev = static_cast<bsp::chrono *>(h->user_dev0);
  return chrono_dev->time_ms();
}

int
at86rf215_spi_read(struct at86rf215 *h, uint8_t *out, const uint8_t *in,
                   size_t tx_len, size_t rx_len)
{
  bsp::spi *spi_dev = static_cast<bsp::spi *>(h->spi_dev);
  spi_dev->read(out, in, tx_len, rx_len);
  return AT86RF215_OK;
}

int
at86rf215_spi_write(struct at86rf215 *h, const uint8_t *in, size_t len)
{
  bsp::spi *spi_dev = static_cast<bsp::spi *>(h->spi_dev);
  spi_dev->write(in, len);
  return AT86RF215_OK;
}

int
at86rf215_irq_user_callback(struct at86rf215 *h, uint8_t rf09_irqs,
                            uint8_t rf24_irqs, uint8_t bbc0_irqs,
                            uint8_t bbc1_irqs)
{
  radio *r = reinterpret_cast<radio *>(h->user_dev1);

  /* Check if a frame has been received */
  if (bbc0_irqs & bit<uint8_t, 1>()) {
    // FIXME USE DMA!
    at86rf215_rx_frame(h, AT86RF215_RF09, rx_msg.pdu, r->m_frame_size);
    rx_msg.info.len = r->m_frame_size;
    int ret         = r->m_rxq.put_isr(rx_msg);
    if (ret) {
      /* FIFO is full probably */
      r->incr_rx_drop_frames(radio::interface::SBAND);
    }

    /* After the RXFE the AT86 does not enter RX mode */
    at86rf215_set_cmd(h, AT86RF215_CMD_RF_RX, AT86RF215_RF09);
  }

  /* Check if a frame has been received */
  if (bbc1_irqs & bit<uint8_t, 1>()) {
    // FIXME USE DMA!
    at86rf215_rx_frame(h, AT86RF215_RF09, rx_msg.pdu, r->m_frame_size);
    rx_msg.info.len = r->m_frame_size;
    int ret         = r->m_rxq.put_isr(rx_msg);
    if (ret) {
      /* FIFO is full probably */
      r->incr_rx_drop_frames(radio::interface::SBAND);
    }

    /* After the RXFE the AT86 does not enter RX mode */
    at86rf215_set_cmd(h, AT86RF215_CMD_RF_RX, AT86RF215_RF24);
  }
  return AT86RF215_OK;
}
}

/**
 * @brief Converts the radio interface to the AT86RF215 equivalent
 *
 * @param iface radio interface
 * @return at86rf215_radio_t the AT86RF215 interface
 */
static at86rf215_radio_t
iface_to_at86_radio(radio::interface iface)
{
  return iface == radio::interface::UHF ? AT86RF215_RF09 : AT86RF215_RF24;
}

/**
 * @brief Construct a new radio::radio object
 *
 */
radio::radio(const io_conf &cnf, power &pwr, bsp::imsgq<rx_msg> &rx_msgq)
    : m_spi(cnf.spi_ctrl),
      m_nrst(cnf.nreset),
      m_iface_enabled{false, false},
      m_rffe09(cnf.rffe09_io, pwr),
      m_rffe24(cnf.rffe24_io, pwr),
      m_chrono(cnf.chrono),
      m_pwr(pwr),
      m_rxq(rx_msgq),
      m_frame_size(0)
{
  bsp_setup();
  at86rf215_enable(false);
}

/**
 * @brief The IRQ handler of the AT86RF215 IRQ
 *
 * @attention The handling of this static method is platform dependent. Users
 * should call this method inside the IRQ handling routine of the target
 * platform
 */
void
radio::trx_irq_handler()
{
  auto &r = board::get_instance().radio();
  at86rf215_irq_callback(&r.m_at86);
}

/**
 * @brief Enable/disable the radio chain.
 * @attention This method does not enable the RF front-ends.
 * It powers up the RF 5V power supply, allowing then for each RF front-end
 * to be enabled separately.
 *
 * @note To avoid accidental activation for any front-end after an
 * enable/disable cycle, when a disable is requested,
 * the RF front-ends are explicitly disabled
 *
 * @param yes true to enable, false to disable
 */
void
radio::enable(bool yes)
{
  if (yes) {
    m_pwr.enable(power::subsys::RF_5V, true);
    at86rf215_enable(true);
    at86rf215_irq_clear(&m_at86);
  } else {
    /*
     * Disabling the whole chain is critical. Set explicitly the RF front-ends
     * as disabled and disable the corresponding power supplies
     */
    m_iface_enabled[static_cast<uint8_t>(interface::UHF)]   = false;
    m_iface_enabled[static_cast<uint8_t>(interface::SBAND)] = false;
    m_rffe09.enable(false);
    m_rffe24.enable(false);
    m_pwr.enable(power::subsys::RF_5V, false);
  }
}

/**
 * @brief Enable/disable the radio chain of a specific band.
 *
 *
 * If the interface is already enabled, this method has no effect. If not,
 * it enables and set the default direction which is \ref direction::RX
 *
 * @param iface the target interface (UHF/S-Band)
 * @param yes true to enable, false to disable
 */
void
radio::enable(interface iface, bool yes)
{
  int ret = 0;
  /* Enable the radio if it is not already enabled */
  if (yes && enabled() == false) {
    enable();
  }
  switch (iface) {
  case interface::UHF:
    if (yes) {
      ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TXPREP, AT86RF215_RF09);
    } else {
      ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TRXOFF, AT86RF215_RF09);
    }
    ret = at86rf215_radio_irq_clear(&m_at86, AT86RF215_RF09);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);
    m_rffe09.enable(yes);
    break;
  case interface::SBAND:
    if (yes) {
      ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TXPREP, AT86RF215_RF24);
    } else {
      ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TRXOFF, AT86RF215_RF24);
    }
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);
    m_rffe24.enable(yes);
    break;
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
    break;
  }
  m_iface_enabled[static_cast<uint8_t>(iface)] = yes;
}

/**
 * @brief Checks if the radio transceiver is in an
 * operational state. This means that their power sources are enabled and each
 * component has been successfully initialized and there is basic connectivity
 * with the radio ICs
 *
 * @return true if all of the components of the radio chain are operational
 * @return false if at least one of the components of the radio is not
 * operational
 */
bool
radio::enabled() const
{
  return m_pwr.enabled(power::subsys::RF_5V) &&
         at86rf215_conn_check(&m_at86) == AT86RF215_OK;
}

bool
radio::enabled(interface iface) const
{
  return enabled() && m_iface_enabled[static_cast<uint8_t>(iface)];
}

void
radio::set_direction(interface iface, rf_frontend::dir d)
{
  /* If the interface is not enabled, enable it! */
  if (enabled(iface) == false) {
    enable(iface);
  }

  switch (iface) {
  case interface::UHF:
    m_rffe09.set_direction(d);
    break;
  case interface::SBAND:
    m_rffe24.set_direction(d);
    break;
  default:
    break;
  }
}

rf_frontend::dir
radio::direction(interface iface) const
{
  switch (iface) {
  case interface::UHF:
    return m_rffe09.direction();
    break;
  case interface::SBAND:
    return m_rffe24.direction();
    break;
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
    break;
  }
}

void
radio::set_frequency(interface iface, rf_frontend::dir d, float freq)
{
  int ret = 0;
  switch (iface) {
  case interface::UHF: {
    if (m_rffe09.frequency_valid(d, freq) == false) {
      SATNOGS_COMMS_ERROR(unsupported_freq_exception);
    }

    struct at86rf215_radio_conf cnf = {.cm  = AT86RF215_CM_FINE_RES_04,
                                       .lbw = AT86RF215_PLL_LBW_DEFAULT};
    ret = at86rf215_radio_conf(&m_at86, AT86RF215_RF09, &cnf);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);

    ret = at86rf215_set_freq(&m_at86, AT86RF215_RF09, freq);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);
  } break;
  case interface::SBAND: {
    if (m_rffe24.frequency_valid(d, freq) == false) {
      SATNOGS_COMMS_ERROR(unsupported_freq_exception);
    }
    struct at86rf215_radio_conf cnf = {.cm  = AT86RF215_CM_FINE_RES_24,
                                       .lbw = AT86RF215_PLL_LBW_DEFAULT};
    ret = at86rf215_radio_conf(&m_at86, AT86RF215_RF24, &cnf);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);

    ret = at86rf215_set_freq(&m_at86, AT86RF215_RF24,
                             rf_frontend24::mixing_freq - freq);
    SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);
  } break;
  }
}

float
radio::frequency(interface iface, rf_frontend::dir d) const
{
  // TODO
  return 0.0;
}

void
radio::set_test_fsk(interface iface)
{
  const at86rf215_radio_t r = iface_to_at86_radio(iface);
  int ret = at86rf215_set_pac(&m_at86, r, AT86RF215_PACUR_NO_RED, 25);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);

  ret = at86rf215_set_radio_irq_mask(&m_at86, r, TX_RF_IRQM);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);
  ret = at86rf215_set_bbc_irq_mask(&m_at86, r, TX_BB_IRQM);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);

  /* FSK-2 50kHz */
  struct at86rf215_bb_conf at86_bb;
  at86_bb.ctx                    = 0;
  at86_bb.fcsfe                  = 0;
  at86_bb.txafcs                 = 1;
  at86_bb.fcst                   = AT86RF215_FCS_16;
  at86_bb.pt                     = AT86RF215_BB_MRFSK;
  at86_bb.fsk.mord               = AT86RF215_2FSK;
  at86_bb.fsk.midx               = AT86RF215_MIDX_3;
  at86_bb.fsk.midxs              = AT86RF215_MIDXS_88;
  at86_bb.fsk.bt                 = AT86RF215_FSK_BT_10;
  at86_bb.fsk.srate              = AT86RF215_FSK_SRATE_50;
  at86_bb.fsk.fi                 = 0;
  at86_bb.fsk.rxo                = AT86RF215_FSK_RXO_DISABLED;
  at86_bb.fsk.pdtm               = 0;
  at86_bb.fsk.rxpto              = 1;
  at86_bb.fsk.mse                = 0;
  at86_bb.fsk.pri                = 0;
  at86_bb.fsk.fecs               = AT86RF215_FSK_FEC_RSC;
  at86_bb.fsk.fecie              = 0;
  at86_bb.fsk.sfd_threshold      = 15;
  at86_bb.fsk.preamble_threshold = 8;
  at86_bb.fsk.sfdq               = 1;
  at86_bb.fsk.sfd32              = 1;
  at86_bb.fsk.rawrbit            = 0;
  at86_bb.fsk.csfd1              = AT86RF215_SFD_UNCODED_RAW;
  at86_bb.fsk.csfd0              = AT86RF215_SFD_UNCODED_RAW;
  at86_bb.fsk.preamble_length    = 16;
  /* SFD operates in a LSB order*/
  at86_bb.fsk.sfd0 = static_cast<uint16_t>(etl::reverse_bits(dsp::ccsds::ASM));
  at86_bb.fsk.sfd1 =
      static_cast<uint16_t>(etl::reverse_bits(dsp::ccsds::ASM) >> 16);
  at86_bb.fsk.sfd         = 0;
  at86_bb.fsk.dw          = 0;
  at86_bb.fsk.preemphasis = 0;
  at86_bb.fsk.dm          = 1;

  ret = at86rf215_bb_conf(&m_at86, r, &at86_bb);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);

  ret = at86rf215_set_mode(&m_at86, AT86RF215_RF_MODE_BBRF);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);

  ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TXPREP, r);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);

  ret = at86rf215_irq_clear(&m_at86);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);

  ret = at86rf215_bb_enable(&m_at86, r, 1);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);
}

void
radio::tx(interface iface, const uint8_t *b, size_t len)
{
  const at86rf215_radio_t r = iface_to_at86_radio(iface);

  int ret = at86rf215_tx_frame(&m_at86, r, b, len, 4000);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);
  incr_tx_frames(iface);
}

void
radio::rx_async(interface iface)
{
  enable(iface);
  set_direction(iface, rf_frontend::dir::RX);
  const at86rf215_radio_t r = iface_to_at86_radio(iface);

  // FIXME: This should originate from the user configuration
  m_frame_size = 64;

  int ret = at86rf215_set_radio_irq_mask(&m_at86, r, RX_RF_IRQM);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);
  ret = at86rf215_set_bbc_irq_mask(&m_at86, r, RX_BB_IRQM);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);

  /* FSK-2 50kHz */
  struct at86rf215_bb_conf at86_bb;
  at86_bb.ctx                    = 0;
  at86_bb.fcsfe                  = 0;
  at86_bb.txafcs                 = 1;
  at86_bb.fcst                   = AT86RF215_FCS_16;
  at86_bb.pt                     = AT86RF215_BB_MRFSK;
  at86_bb.fsk.mord               = AT86RF215_2FSK;
  at86_bb.fsk.midx               = AT86RF215_MIDX_3;
  at86_bb.fsk.midxs              = AT86RF215_MIDXS_88;
  at86_bb.fsk.bt                 = AT86RF215_FSK_BT_10;
  at86_bb.fsk.srate              = AT86RF215_FSK_SRATE_50;
  at86_bb.fsk.fi                 = 0;
  at86_bb.fsk.rxo                = AT86RF215_FSK_RXO_DISABLED;
  at86_bb.fsk.pdtm               = 0;
  at86_bb.fsk.rxpto              = 1;
  at86_bb.fsk.mse                = 0;
  at86_bb.fsk.pri                = 0;
  at86_bb.fsk.fecs               = AT86RF215_FSK_FEC_RSC;
  at86_bb.fsk.fecie              = 0;
  at86_bb.fsk.sfd_threshold      = 8;
  at86_bb.fsk.preamble_threshold = 8;
  at86_bb.fsk.sfdq               = 0;
  at86_bb.fsk.sfd32              = 1;
  at86_bb.fsk.rawrbit            = 1; // Most of the encoders transmit MSB first
  at86_bb.fsk.csfd1              = AT86RF215_SFD_UNCODED_RAW;
  at86_bb.fsk.csfd0              = AT86RF215_SFD_UNCODED_RAW;
  at86_bb.fsk.preamble_length    = 8;
  /* SFD operates in a LSB order*/
  at86_bb.fsk.sfd0 = static_cast<uint16_t>(etl::reverse_bits(dsp::ccsds::ASM));
  at86_bb.fsk.sfd1 =
      static_cast<uint16_t>(etl::reverse_bits(dsp::ccsds::ASM) >> 16);
  at86_bb.fsk.sfd         = 0;
  at86_bb.fsk.dw          = 0;
  at86_bb.fsk.preemphasis = 0;
  at86_bb.fsk.dm          = 1;

  ret = at86rf215_bb_conf(&m_at86, r, &at86_bb);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);

  ret = at86rf215_set_mode(&m_at86, AT86RF215_RF_MODE_BBRF);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);

  ret = at86rf215_irq_clear(&m_at86);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);

  ret = at86rf215_bb_enable(&m_at86, r, 1);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);

  ret = at86rf215_rx(&m_at86, r, 1000);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);
}

/**
 * @brief Gets the status of the RF mixer lock
 *
 * @return true if the mixer is locked
 * @return false if the mixer is not locked or does not communicate
 */
bool
radio::mixer_lock()
{
  return m_rffe24.mixer_lock();
}

/**
 *
 * @return rf_frontend09& of the UHF RF front-end
 */
rf_frontend09 &
radio::uhf()
{
  return m_rffe09;
}

/**
 *
 * @return rf_frontend24& of the S-band RF front-end
 */
rf_frontend24 &
radio::sband()
{
  return m_rffe24;
}

/**
 * @brief Gets an available message from the receive message queue
 *
 * @param msg reference to place the received message
 * @param timeout_ms the timeout in milliseconds
 * @return 0 on success or negative error code on failure or timeout
 */
int
radio::recv_msg(rx_msg &msg, size_t timeout_ms)
{
  int ret = m_rxq.get(&msg, timeout_ms);
  if (ret) {
    return ret;
  }

  /* Check the CRC */
  auto crc =
      etl::crc32_c(msg.pdu, msg.pdu + msg.info.len - sizeof(uint32_t)).value();
  uint32_t recv = (msg.pdu[msg.info.len - sizeof(uint32_t)] << 24) |
                  (msg.pdu[msg.info.len - sizeof(uint32_t) + 1] << 16) |
                  (msg.pdu[msg.info.len - sizeof(uint32_t) + 2] << 8) |
                  msg.pdu[msg.info.len - sizeof(uint32_t) + 3];
  if (crc == recv) {
    incr_rx_frames(msg.info.iface);
    return 0;
  }

  incr_rx_inval_frames(msg.info.iface);
  // FIXME Define error codes?
  return -1;
}

/**
 * @brief Gets the statistics for a specific interface
 *
 * @param iface the desired interface
 * @return const radio::stats& holding the statistics of the corresponding
 * interface
 */
const radio::stats &
radio::get_stats(interface iface) const
{
  return m_stats[static_cast<uint8_t>(iface)];
}

/**
 * Internal structures initialization.
 *
 * @warning Touch it only if you know what you are doing!
 *
 */
void
radio::bsp_setup()
{
  m_at86.spi_dev      = &m_spi;
  m_at86.cs_gpio_dev  = &m_spi.cs();
  m_at86.rst_gpio_dev = &m_nrst;
  /* We use the user0 field for the chrono related stuff */
  m_at86.user_dev0 = &m_chrono;
  /*
   * user1 has the class itself for handling the user defined IRQ handler of the
   * AT86RF215 driver
   */
  m_at86.user_dev1 = this;
}

/**
 * @brief Enable/disable the IC
 *
 * @param enable true to enable, false otherwise
 */
void
radio::at86rf215_enable(bool enable)
{
  /* In case the user needs to disable the IC, just de-assert the RSTN pin */
  if (enable == false) {
    at86rf215_set_rstn(&m_at86, 0);
    return;
  }

  m_at86.clk_drv      = AT86RF215_RF_DRVCLKO8;
  m_at86.clko_os      = AT86RF215_RF_CLKO_26_MHZ;
  m_at86.xo_trim      = 0;
  m_at86.xo_fs        = 0;
  m_at86.irqp         = 0;
  m_at86.irqmm        = 0;
  m_at86.pad_drv      = AT86RF215_RF_DRV8;
  m_at86.rf_femode_09 = AT86RF215_RF_FEMODE2;
  m_at86.rf_femode_24 = AT86RF215_RF_FEMODE2;

  int ret = at86rf215_init(&m_at86);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);
  ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TRXOFF, AT86RF215_RF09);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);
  ret = at86rf215_set_cmd(&m_at86, AT86RF215_CMD_RF_TRXOFF, AT86RF215_RF24);
  SATNOGS_COMMS_ASSERT_ERROR_CODE(ret, radio_exception);
}

void
radio::incr_tx_frames(interface iface)
{
  if (m_stats[static_cast<uint8_t>(iface)].tx_frames <
      std::numeric_limits<size_t>::max()) {
    m_stats[static_cast<uint8_t>(iface)].tx_frames++;
  }
}

void
radio::incr_tx_fail_frames(interface iface)
{
  if (m_stats[static_cast<uint8_t>(iface)].tx_frames_fail <
      std::numeric_limits<size_t>::max()) {
    m_stats[static_cast<uint8_t>(iface)].tx_frames_fail++;
  }
}

void
radio::incr_tx_drop_frames(interface iface)
{
  if (m_stats[static_cast<uint8_t>(iface)].tx_frames_drop <
      std::numeric_limits<size_t>::max()) {
    m_stats[static_cast<uint8_t>(iface)].tx_frames_drop++;
  }
}

void
radio::incr_rx_frames(interface iface)
{
  if (m_stats[static_cast<uint8_t>(iface)].rx_frames <
      std::numeric_limits<size_t>::max()) {
    m_stats[static_cast<uint8_t>(iface)].rx_frames++;
  }
}

void
radio::incr_rx_inval_frames(interface iface)
{
  if (m_stats[static_cast<uint8_t>(iface)].rx_frames_inval <
      std::numeric_limits<size_t>::max()) {
    m_stats[static_cast<uint8_t>(iface)].rx_frames_inval++;
  }
}

void
radio::incr_rx_drop_frames(interface iface)
{
  if (m_stats[static_cast<uint8_t>(iface)].rx_frames_drop <
      std::numeric_limits<size_t>::max()) {
    m_stats[static_cast<uint8_t>(iface)].rx_frames_drop++;
  }
}

} // namespace satnogs::comms
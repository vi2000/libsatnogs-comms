/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/leds.hpp>

namespace satnogs::comms
{

leds::leds(bsp::gpio &led0, bsp::gpio &led1)
    : m_leds({{led::led0, led0}, {led::led1, led1}})
{
}

void
leds::toggle(led x)
{
  m_leds.at(x).toggle();
}

void
leds::enable(led x, bool en)
{
  m_leds.at(x).set(en);
}

void
leds::disable(led x)
{
  enable(x, false);
}

bool
leds::enabled(led x) const
{
  return m_leds.at(x).get();
}

} // namespace satnogs::comms

/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/ina322x.hpp>

#define INA3221_MANUFACTURER_ID     0xFE
#define INA3221_READ                0x01
#define INA3221_REG_CONFIG          0x00
#define INA3221_CONFIG_RESET        0x8000 // Reset Bit
#define INA3221_CONFIG_ENABLE_CHAN1 0x4000 // Enable Channel 1
#define INA3221_CONFIG_ENABLE_CHAN2 0x2000 // Enable Channel 2
#define INA3221_CONFIG_ENABLE_CHAN3 0x1000 // Enable Channel 3
#define INA3221_CONFIG_AVG2         0x0800 // AVG Samples Bit 2 - See table 3 spec
#define INA3221_CONFIG_AVG1         0x0400 // AVG Samples Bit 1 - See table 3 spec
#define INA3221_CONFIG_AVG0         0x0200 // AVG Samples Bit 0 - See table 3 spec
#define INA3221_CONFIG_VBUS_CT2                                                \
  0x0100 // VBUS bit 2 Conversion time - See table 4 spec
#define INA3221_CONFIG_VBUS_CT1                                                \
  0x0080 // VBUS bit 1 Conversion time - See table 4 spec
#define INA3221_CONFIG_VBUS_CT0                                                \
  0x0040 // VBUS bit 0 Conversion time - See table 4 spec
#define INA3221_CONFIG_VSH_CT2                                                 \
  0x0020 // Vshunt bit 2 Conversion time - See table 5 spec
#define INA3221_CONFIG_VSH_CT1                                                 \
  0x0010 // Vshunt bit 1 Conversion time - See table 5 spec
#define INA3221_CONFIG_VSH_CT0                                                 \
  0x0008 // Vshunt bit 0 Conversion time - See table 5 spec
#define INA3221_CONFIG_MODE_2      0x0004 // Operating Mode bit 2 - See table 6 spec
#define INA3221_CONFIG_MODE_1      0x0002 // Operating Mode bit 1 - See table 6 spec
#define INA3221_CONFIG_MODE_0      0x0001 // Operating Mode bit 0 - See table 6 spec
#define INA3221_REG_SHUNTVOLTAGE_1 0x01
#define INA3221_REG_BUSVOLTAGE_1   0x02
#define INA3221_REG_SHUNTVOLTAGE_2 0x03
#define INA3221_REG_BUSVOLTAGE_2   0x04
#define INA3221_REG_SHUNTVOLTAGE_3 0x05
#define INA3221_REG_BUSVOLTAGE_3   0x06
#define INA3221_ID                 0x5449

namespace satnogs::comms
{

ina322x::ina322x(bsp::i2c &i2c, uint16_t addr, float ch1_shunt, float ch2_shunt,
                 float ch3_shunt)
    : m_i2c(i2c),
      m_addr(addr),
      m_ch1_shunt(ch1_shunt),
      m_ch2_shunt(ch2_shunt),
      m_ch3_shunt(ch3_shunt)
{
  reset();
}

/**
 * @brief Returns the current of a channel in amperes
 *
 * @param ch the desired channel
 * @return the current in amperes
 */
float
ina322x::current(channel ch) const
{
  uint8_t reg   = 0;
  float   shunt = 0.0f;
  switch (ch) {
  case channel::CH1:
    reg   = INA3221_REG_SHUNTVOLTAGE_1;
    shunt = m_ch1_shunt;
    break;
  case channel::CH2:
    reg   = INA3221_REG_SHUNTVOLTAGE_2;
    shunt = m_ch2_shunt;
    break;
  case channel::CH3:
    reg   = INA3221_REG_SHUNTVOLTAGE_1;
    shunt = m_ch3_shunt;
    break;
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
    break;
  }

  int16_t value = (int16_t)read_reg(reg);
  return (value * 0.005f) / shunt / 1000.0f;
}

/**
 * @brief Returns the voltage of a channel in volts
 *
 * @param ch the desired channel
 * @return the voltage in volts
 */
float
ina322x::voltage(channel ch) const
{
  uint8_t reg = 0;
  switch (ch) {
  case channel::CH1:
    reg = INA3221_REG_BUSVOLTAGE_1;
    break;
  case channel::CH2:
    reg = INA3221_REG_BUSVOLTAGE_2;
    break;
  case channel::CH3:
    reg = INA3221_REG_BUSVOLTAGE_3;
    break;
  default:
    SATNOGS_COMMS_ERROR(inval_arg_exception);
    break;
  }
  uint16_t value = read_reg(reg);
  return value / 1000.0f;
}

void
ina322x::reset()
{

  /* Check if we can communicate with the IC */
  uint16_t val = read_reg(INA3221_MANUFACTURER_ID);
  if (val != INA3221_ID) {
    SATNOGS_COMMS_ERROR(ina322x_comm_exception);
  }

  /*
   * All channels enabled
   * 256 samples averaging
   * 1.1 ms conversion time
   */
  uint16_t config = INA3221_CONFIG_ENABLE_CHAN1 | INA3221_CONFIG_ENABLE_CHAN2 |
                    INA3221_CONFIG_ENABLE_CHAN3 | INA3221_CONFIG_AVG2 |
                    INA3221_CONFIG_AVG0 | INA3221_CONFIG_VBUS_CT2 |
                    INA3221_CONFIG_VSH_CT2 | INA3221_CONFIG_MODE_2 |
                    INA3221_CONFIG_MODE_1 | INA3221_CONFIG_MODE_0;
  write_reg(INA3221_REG_CONFIG, config);
}

void
ina322x::write_reg(uint8_t reg, uint16_t value) const
{
  uint8_t buf[2] = {static_cast<uint8_t>(value & 0xff),
                    static_cast<uint8_t>(value >> 8)};
  m_i2c.write(m_addr, reg, buf, 2);
}

uint16_t
ina322x::read_reg(uint8_t reg) const
{
  uint8_t buf[2] = {0, 0};

  m_i2c.read(m_addr, reg, buf, 2);
  uint16_t val = (buf[0] << 8) | buf[1];
  return val;
}

} // namespace satnogs::comms

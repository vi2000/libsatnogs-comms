/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2021-2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/error.hpp>
#include <satnogs-comms/exception.hpp>
#include <satnogs-comms/lp5912.hpp>

namespace satnogs::comms
{

lp5912::lp5912(const etl::istring &name, bsp::gpio *on, bsp::gpio *pgood)
    : m_name(name), m_on_gpio(on), m_pgood_gpio(pgood)
{
  // FIXME
  if (!on) {
    return;
    // SATNOGS_COMMS_ERROR(inval_arg_exception);
  }
  enable(false);
}

const etl::istring &
lp5912::name() const
{
  return m_name;
}

/**
 * Enables/Disables the LP5912
 * @param set set to true to enable or false to disable
 */
void
lp5912::enable(bool set)
{
  m_on_gpio->set(set);
}

/**
 * Return the power state of LP5912
 * @return true if the IC is ON, false otherwise
 */
bool
lp5912::enabled() const
{
  return m_on_gpio->get();
}

/**
 * Return the power-good indicator
 * @return true if the IC is working properly, false otherwise
 */
bool
lp5912::pgood() const
{
  if (m_pgood_gpio) {
    return m_pgood_gpio->get();
  }
  return false;
}

} // namespace satnogs::comms

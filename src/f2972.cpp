/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2021-2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/error.hpp>
#include <satnogs-comms/f2972.hpp>

namespace satnogs::comms
{

/**
 * Creates an object for controlling the F2972 RF-switch. If the initialization
 * is successful the switch is set to a disabled state. In this state port ports
 * are terminated. However, if the switch is enabled, the first selected port
 * will be the RF1, unless the user specifies another one.
 *
 * @param name a descriptive name of the switch
 * @param en the GPIO of the EN pin
 * @param ctl the GPIO of the CTL pin
 */
f2972::f2972(const etl::istring &name, bsp::gpio &en, bsp::gpio &ctl)
    : m_name(name), m_en_gpio(en), m_ctrl_gpio(ctl), m_port(port::RF1)
{
  enable(false);
  set_port(m_port);
}

/**
 *
 * @return the name of the switch
 */
const etl::istring &
f2972::name() const
{
  return m_name;
}

/**
 * Enables/Disables the RF-switch
 * @param set if set to true the switch is enabled, otherwise it is disabled
 */
void
f2972::enable(bool set)
{
  m_en_gpio.set(set);
}

/**
 *
 * @return true if the switch is enabled, false otherwise
 */
bool
f2972::enabled() const
{
  return m_en_gpio.get();
}

/**
 * Selects the RF-switch port
 * @param p the port to connect with the RFC
 */
void
f2972::set_port(port p)
{
  switch (p) {
  case port::RF1:
    m_ctrl_gpio.set(false);
    break;
  case port::RF2:
    m_ctrl_gpio.set(true);
    break;
  default:
    SATNOGS_COMMS_ERROR(f2972_inval_param_exception);
  }
  m_port = p;
}

/**
 *
 * @return the selected port. If the switch is disabled, the return value does
 * not makes sense. Check the datasheet for more details
 */
f2972::port
f2972::active_port() const
{
  return m_port;
}

} // namespace satnogs::comms

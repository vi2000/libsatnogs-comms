# SatNOGS COMMS Control Library
SatNOGS COMMS Control Library is an interface library that controls the
[SatNOGS COMMS](https://gitlab.com/librespacefoundation/satnogs-comms) transceiver.
This library relies a peripheral abstraction layer, allowing users of the
[SatNOGS COMMS](https://gitlab.com/librespacefoundation/satnogs-comms) to build
their own firmware with minimal effort.
The official software of the transceiver utilizing this library and Zephyr-RTOS
can be found at the
[SatNOGS COMMS Software MCU](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu) repository.

![SatNOGS-COMMS](docs/assets/pcb-top.png)

## Usage
The SatNOGS-COMMS Control library is a CMake Interface library, exposed under the `libsatnogs-comms` target.
This targets provides all the necessary source files and the include directories.

The only requirement of the library is the hardware version.
Users should set the variable `HW_VERSION` before the usage of the library.
For more information reqrding the available hardware versions refer to the [available hardware revisions](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-hardware/-/tags).

Integration in a CMake-based project is straightforward, using the `FetchContent()`.

```cmake
# Set the HW_VERSION variable required by the libsatnogs-comms
set (HW_VERSION "0.3.0" CACHE STRING "")

include(FetchContent)
FetchContent_Declare (
  satnogs-comms-ctrl-lib
  SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/libsatnogs-comms"
  BINARY_DIR "${CMAKE_CURRENT_BINARY_DIR}/libsatnogs-comms")
FetchContent_MakeAvailable(satnogs-comms-ctrl-lib)
```

After this, the last step is to link the desired executable with the `libsatnogs-comms` target.

```cmake
target_link_libraries(app PRIVATE libsatnogs-comms)
```
Initialization and usage of the board is quite easy.
Before any usage of the control library, the `init()` static method of the [board class](include/satnogs-comms/board.hpp) should be called. For example:

```cpp
power::io_conf pwr_cnf = {/* IO configuration*/};
rf_frontend09::io_conf rffe09_io = {/* IO configuration*/};
rf_frontend24::io_conf rffe24_io = {/* IO configuration*/};
radio::io_conf radio_cnf = {/* IO configuration*/};
board::io_conf board_io = {/* IO configuration*/};
board::params board_params = {/* parameters */};

/* Init should be called once! */
board::init(board_io, board_params);
```

The `board` class uses the singleton pattern. This ensures that only a single instance for the entire firmware exists.
To get access to this instance the `get_instance()` static method can be used.
The `board` class gives access to all the subsystems and has properly created them and initialized during the `init()`.

**No manual creation of classes or subsystems is needed.**

For example:

```cpp
auto &brd = board::get_instance();
auto &radio = brd.radio();
radio.enable();
radio.enable(sc::radio::interface::UHF, false);
radio.enable(sc::radio::interface::SBAND, false);

auto &fpga = brd.fpga();
fpga.enable();
```

## Development Guide
SatNOGS COMMS Control Library is independent of any kind of IDE.
You can use the development environment of your choice.

The codebase contains also a standalone and fully functional test firmware,
under the `test` directory, that can be flashed directly to the board using openOCD.

### Requirements
* CMake (>= 3.20)
* clang-format (>= 17.0)
* C++17

#### Optional requirements
* openOCD (>= 0.11) (for testing)

### Coding Style
For the C and C++ code, we use **LLVM** style.
Use `clang-format` and the options file `.clang-format` to
adapt to the styling.

At the root directory of the project there is the `clang-format` options
file `.clang-format` containing the proper configuration.
Developers can import this configuration to their favorite editor.
Failing to comply with the coding style described by the `.clang-format`
will result to failure of the automated tests running on our CI services.
So make sure that you import on your editor the coding style rules.

## Website and Contact
For more information about the project and Libre Space Foundation please visit our [site](https://libre.space/)
and our [community forums](https://community.libre.space).

## License
![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png)
&copy; 2024 [Libre Space Foundation](https://libre.space).

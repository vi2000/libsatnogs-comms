/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <etl/string.h>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/bsp/i2c.hpp>
#include <satnogs-comms/error.hpp>
#include <satnogs-comms/exception.hpp>

namespace satnogs::comms
{
class tmp117
{
public:
  static constexpr float tmp117_resolution = 0.0078125f;

  struct config
  {
    bool    high_alert    = false;
    bool    low_alert     = false;
    bool    data_ready    = false;
    bool    eeprom_busy   = false;
    uint8_t mod           = 0;
    uint8_t conv          = 0;
    uint8_t avg           = 0;
    bool    term_or_alert = false;
    bool    pol           = false;
    bool    dr_or_alert   = false;
    bool    soft_reset    = false;
  };

  enum class sensor_mode : uint8_t
  {
    CONTINUOUS = 0b00,
    SHUTDOWN   = 0b01,
    ONESHOT    = 0b11
  };

  enum class conversion_time : uint8_t
  {
    CC_15_MS_5 = 0b000,
    CC_125_MS  = 0b001,
    CC_250_MS  = 0b010,
    CC_500_MS  = 0b011,
    CC_1_S     = 0b100,
    CC_4_S     = 0b101,
    CC_8_S     = 0b110,
    CC_16_S    = 0b111
  };

  enum class averaging_mode : uint8_t
  {
    NO_AVE = 0b00,
    AVE8   = 0b01,
    AVE32  = 0b10,
    AVE64  = 0b11
  };

  enum class pin_mode : uint8_t
  {
    ALERT   = 0b0,
    THERMAL = 0b1
  };

  enum class pin_polarity : uint8_t
  {
    ACTIVE_LOW  = 0b0,
    ACTIVE_HIGH = 0b1
  };

  enum class alert_pin_select : uint8_t
  {
    ALERT_FLAG      = 0b0,
    DATA_READY_FLAG = 0b1
  };

  tmp117(const etl::istring &name, bsp::i2c &i2c, uint16_t addr,
         bsp::gpio &alert);

  tmp117(const etl::istring &name, bsp::i2c &i2c, uint16_t addr);

  void
  set_config() const;

  float
  temperature() const;

  bool
  alert() const;

  void
  get_status(config &c) const;

  void
  set_temp_lim(float low_lim = -256.0f, float high_lim = 255.0f) const;

  bool
  get_eeprom_busy() const;

  void
  unlock_eeprom() const;

  void
  lock_eeprom() const;

  uint16_t
  read_eeprom(uint8_t eeprom_number) const;

  void
  write_eeprom(uint8_t eeprom_number, uint16_t val) const;

  void
  set_offset_temp(float offset = 0.0f) const;

  float
  get_offset_temp() const;

  uint16_t
  get_device_id(void) const;

  uint8_t
  get_device_rev(void) const;

private:
  etl::string<32>  m_name;
  bsp::i2c        &m_i2c;
  const uint32_t   m_addr;
  bsp::gpio       &m_alert;
  bsp::dummy_gpio  m_dummy;
  config           c;
  sensor_mode      mode;
  conversion_time  conv;
  averaging_mode   avrg;
  pin_mode         pin_mod;
  pin_polarity     pin_pol;
  alert_pin_select a_p_s;

  static void
  set_sensor_mode(sensor_mode m, uint8_t &high_byte, uint8_t &low_byte);

  static void
  set_conv_time(conversion_time t, uint8_t &high_byte, uint8_t &low_byte);

  static void
  set_averaging(averaging_mode a, uint8_t &high_byte, uint8_t &low_byte);

  static void
  set_pin_mode(pin_mode p_m, uint8_t &high_byte, uint8_t &low_byte);

  static void
  set_pin_polarity(pin_polarity p_p, uint8_t &high_byte, uint8_t &low_byte);

  static void
  set_alert_pin_select(alert_pin_select a_p, uint8_t &high_byte,
                       uint8_t &low_byte);
};

class tmp117_inval_param_exception : public exception
{
public:
  tmp117_inval_param_exception(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("tmp117 invalid param", "tmp117invalparam"),
                  file_name, line)
  {
  }
};

class tmp117_not_ready : public exception
{
public:
  tmp117_not_ready(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("The EEPROM is busy", "busy"), file_name, line)
  {
  }
};

} // namespace satnogs::comms

/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef INCLUDE_SATNOGS_COMMS_EMMC_HPP_
#define INCLUDE_SATNOGS_COMMS_EMMC_HPP_

#include <etl/string.h>
#include <satnogs-comms/bsp/gpio.hpp>

namespace satnogs::comms
{

class emmc
{
public:
  /**
    * Direction / interface
    */
  enum class dir
  {
    MCU, /**< MCU interface */
    FPGA /**< FPGA interface */
  };

  emmc(const etl::istring &name, uint64_t total_size, uint64_t start, uint64_t stop,
       bsp::gpio &en, bsp::gpio &sel);

  const etl::istring &
  name() const;

  void
  enable(bool set = true);

  bool
  enabled() const;

  void
  set_dir(dir d);

  dir
  get_dir() const;

  uint64_t
  capacity() const;

  uint64_t
  start() const;

  uint64_t
  stop() const;

private:
  const uint64_t  m_capasity;
  const uint64_t  m_start;
  const uint64_t  m_stop;
  etl::string<32> m_name;
  bsp::gpio      &m_en_gpio;
  bsp::gpio      &m_sel_gpio;
};

} // namespace satnogs::comms

#endif /* INCLUDE_SATNOGS_COMMS_EMMC_HPP_ */

/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2021-2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef INCLUDE_SATNOGS_COMMS_F2972_HPP_
#define INCLUDE_SATNOGS_COMMS_F2972_HPP_

#include <etl/string.h>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/exception.hpp>

namespace satnogs::comms
{

class f2972_inval_param_exception : public exception
{
public:
  f2972_inval_param_exception(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("f2972 invalid param", "f2972invalparam"),
                  file_name, line)
  {
  }
};

/**
 * Class for controlling the F2972 RF-switch
 */
class f2972
{
public:
  enum class port : uint8_t
  {
    RF1,
    RF2
  };

  f2972(const etl::istring &name, bsp::gpio &en, bsp::gpio &ctl);

  const etl::istring &
  name() const;

  void
  enable(bool set = true);

  bool
  enabled() const;

  void
  set_port(port p);

  port
  active_port() const;

private:
  etl::string<32> m_name;
  bsp::gpio      &m_en_gpio;
  bsp::gpio      &m_ctrl_gpio;
  port            m_port;
};

} // namespace satnogs::comms

#endif /* INCLUDE_SATNOGS_COMMS_F2972_HPP_ */

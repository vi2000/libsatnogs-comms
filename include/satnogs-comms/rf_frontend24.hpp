/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef INCLUDE_SATNOGS_COMMS_RF_FRONTEND24_HPP_
#define INCLUDE_SATNOGS_COMMS_RF_FRONTEND24_HPP_

#include <rffcx07x.h>
#include <satnogs-comms/ad8318.hpp>
#include <satnogs-comms/f2972.hpp>
#include <satnogs-comms/lna.hpp>
#include <satnogs-comms/rf_frontend.hpp>

namespace satnogs::comms
{

class rf_frontend24_exception : public exception
{
public:
  rf_frontend24_exception(string_type file_name, numeric_type line, int err)
      : exception(ETL_ERROR_TEXT("radio error", "radioerr"), file_name, line,
                  err)
  {
  }
};

class mixer_lock_exception : public exception
{
public:
  mixer_lock_exception(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("radio error", "radioerr"), file_name, line)
  {
  }
};

class rf_frontend24 : public rf_frontend
{
public:
  static constexpr float mixing_freq = 4680e6;
  class io_conf
  {
  public:
    bsp::gpio   &en_agc;
    bsp::dac    &agc_vset;
    bsp::gpio   &flt_sel;
    bsp::gpio   &mixer_clk;
    bsp::gpio   &mixer_rst;
    bsp::gpio   &mixer_enx;
    bsp::gpio   &mixer_sda;
    bsp::chrono &chrono;
  };

  rf_frontend24(io_conf &io, power &pwr);

  void
  enable(bool set = true);

  bool
  enabled() const;

  void
  set_direction(dir d);

  bool
  mixer_lock();

private:
  bsp::chrono    &m_chrono;
  struct rffcx07x m_mixer;
};

} /* namespace satnogs::comms */

#endif /* INCLUDE_SATNOGS_COMMS_RF_FRONTEND09_HPP_ */

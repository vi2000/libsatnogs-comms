/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstdint>
#include <satnogs-comms/bsp/i2c.hpp>
#include <satnogs-comms/emc1702.hpp>
#include <satnogs-comms/exception.hpp>
#include <satnogs-comms/tmp117.hpp>
#include <satnogs-comms/version.hpp>

namespace satnogs::comms
{

/**
 * Temperature sensor source
 */
enum class temperature_sensor : uint8_t
{
  PCB,      //! PCB temperature sensor
  UHF_PA,   //! UHF PA temperature sensor
  SBAND_PA //! S-Band PA temperature sensor
};

class invalid_sensor_exception : public exception
{
public:
  invalid_sensor_exception(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("invalid sensor error", "invalsenserr"),
                  file_name, line)
  {
  }
};

template <typename T> class temperature
{
public:
  temperature(bsp::i2c &i2c, bsp::gpio &alert_t_pa_uhf,
              bsp::gpio &alert_t_pa_sband, const rf_frontend09 &rf09,
              const rf_frontend24 &rf24)
      : m_pcb(etl::make_string("pcb"), i2c, pcb_addr),
        m_uhf(etl::make_string("pa-uhf"), i2c, uhf_addr, alert_t_pa_uhf),
        m_sband(etl::make_string("pa-sband"), i2c, sband_addr,
                alert_t_pa_sband),
        m_rf09(rf09),
        m_rf24(rf24)

  {
    static_assert(std::is_same_v<emc1702, T> || std::is_same_v<tmp117, T>,
                  "Unsupported temperature sensor. T must be either the "
                  "emc1702 or the tmp117 class");
  }

  // Contructor specialiazation if needed can be applied by using:
  //
  // template <typename X = T, typename std::enable_if_t<std::is_same_v<
  //                               temperature<X>, temperature<emc1702>>>>
  // temperature(bsp::i2c &i2c)
  // {
  // }

  /**
   * @brief
   *
   * @return float with the average of all available temperature sensors
   */
  float
  get() const
  {
    /*
     * RF front-end temperatures are available only if the corresponding
     * front-end is enabled.
     */
    return (get(temperature_sensor::PCB) + get(temperature_sensor::UHF_PA) +
            get(temperature_sensor::SBAND_PA)) /
           3.0f;
  }

  /**
   * @brief Returns the temperature from a specific sensor
   *
   * @param s the target sensor
   * @return float with the value of the temperature
   */
  float
  get(temperature_sensor s) const;

  bool
  alert() const
  {
    return alert(temperature_sensor::PCB) ||
           alert(temperature_sensor::UHF_PA) ||
           alert(temperature_sensor::SBAND_PA);
  }

  bool
  alert(temperature_sensor s) const;

private:
  static constexpr uint8_t pcb_addr   = version::hw() > version::num(0, 2)
                                            ? 0b101101
                                            : 0b1001010;
  static constexpr uint8_t uhf_addr   = version::hw() > version::num(0, 2)
                                            ? 0b1001101
                                            : 0b1001000;
  static constexpr uint8_t sband_addr = version::hw() > version::num(0, 2)
                                            ? 0b11000
                                            : 0b1001011;

  T                    m_pcb;
  T                    m_uhf;
  T                    m_sband;
  const rf_frontend09 &m_rf09;
  const rf_frontend24 &m_rf24;
};

/*
 * Provide full specialization for the tmp117 and the emc1702 as the offer
 * different API. NOTE: The inline is used to avoid the ODR violation. Possible
 * option is to move the full specialization at the .cpp file.
 */

template <>
inline float
temperature<tmp117>::get(temperature_sensor s) const
{
  switch (s) {
  case temperature_sensor::PCB:
    return m_pcb.temperature();
  case temperature_sensor::UHF_PA:
    return m_uhf.temperature();
  case temperature_sensor::SBAND_PA:
    return m_sband.temperature();
  // No support for the AGC temperature sensors at v0.2
  default:
    SATNOGS_COMMS_ERROR(invalid_sensor_exception);
  }
}

template <>
inline bool
temperature<tmp117>::alert(temperature_sensor s) const
{
  switch (s) {
  case temperature_sensor::PCB:
    return m_pcb.alert();
  case temperature_sensor::UHF_PA:
    return m_uhf.alert();
  case temperature_sensor::SBAND_PA:
    return m_sband.alert();
  default:
    SATNOGS_COMMS_ERROR(invalid_sensor_exception);
  }
}

template <>
inline float
temperature<emc1702>::get(temperature_sensor s) const
{
  switch (s) {
  case temperature_sensor::PCB:
    return m_pcb.get_temperature_average();
  case temperature_sensor::UHF_PA:
    return m_uhf.get_temperature_average();
  case temperature_sensor::SBAND_PA:
    return m_sband.get_temperature_average();
  default:
    SATNOGS_COMMS_ERROR(invalid_sensor_exception);
  }
}

template <>
inline bool
temperature<emc1702>::alert(temperature_sensor s) const
{
  switch (s) {
  case temperature_sensor::PCB:
    return m_pcb.alert();
  case temperature_sensor::UHF_PA:
    return m_uhf.alert();
  case temperature_sensor::SBAND_PA:
    return m_sband.alert();
  default:
    SATNOGS_COMMS_ERROR(invalid_sensor_exception);
  }
}

} // namespace satnogs::comms

/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2021-2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef INCLUDE_SATNOGS_COMMS_ERROR_HPP_
#define INCLUDE_SATNOGS_COMMS_ERROR_HPP_

// NOTE: Use an ETL profile file to declare the components that are needed
#include <etl/error_handler.h>

#if defined(ETL_NO_CHECKS)
#error "ETL_NO_CHECKS option is not allowed for libsatnogs-comms"
#else
#define SATNOGS_COMMS_ASSERT_ERROR_CODE(err, e)                                \
  {                                                                            \
    if ((err)) {                                                               \
      etl::error_handler::error((e(__FILE__, __LINE__, err)));                 \
      throw((e(__FILE__, __LINE__, err)));                                     \
    }                                                                          \
  }

#define SATNOGS_COMMS_ERROR(e)                                                 \
  {                                                                            \
    etl::error_handler::error((e(__FILE__, __LINE__)));                        \
    throw((e(__FILE__, __LINE__)));                                            \
  }
#endif

#endif /* INCLUDE_SATNOGS_COMMS_ERROR_HPP_ */

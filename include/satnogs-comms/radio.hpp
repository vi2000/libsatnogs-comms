/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef RADIO_HPP
#define RADIO_HPP

#include <at86rf215.h>
#include <satnogs-comms/bsp/chrono.hpp>
#include <satnogs-comms/bsp/msgq.hpp>
#include <satnogs-comms/bsp/spi.hpp>
#include <satnogs-comms/rf_frontend09.hpp>
#include <satnogs-comms/rf_frontend24.hpp>
#include <satnogs-comms/temperature.hpp>
#include <satnogs-comms/version.hpp>

#include <type_traits>

namespace satnogs::comms
{

class radio_exception : public exception
{
public:
  radio_exception(string_type file_name, numeric_type line, int err)
      : exception(ETL_ERROR_TEXT("radio error", "radioerr"), file_name, line,
                  err)
  {
  }
};

class unsupported_freq_exception : public exception
{
public:
  unsupported_freq_exception(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("unsupported frequency error", "freqerr"),
                  file_name, line)
  {
  }
};

/*
 * This is a weak function of the AT86RF215 driver allowing user defined
 * processing of the IRQs. We need to forward declare it here in order
 * to define it as friend of the radio class.
 * With this trick, this callback can access the private fields of the radio
 * class. This eliminate the need for the radio class to expose at the public
 * API, dangerous/unnecessary methods
 */
extern "C" int
at86rf215_irq_user_callback(struct at86rf215 *h, uint8_t rf09_irqs,
                            uint8_t rf24_irqs, uint8_t bbc0_irqs,
                            uint8_t bbc1_irqs);

class radio
{
public:
  static const uint16_t UHF_AGC_VOUT_DAC_CH   = 2;
  static const uint16_t SBAND_AGC_VOUT_DAC_CH = 1;

  /**
   * Interface type of the radio
   */
  enum class interface : uint8_t
  {
    UHF   = 0, /**< UHF */
    SBAND = 1  /**< S-BAND */
  };

  /**
   * Operational mode
   */
  enum class op_mode : uint8_t
  {
    BASEBAND, /**< The baseband core is active */
    IQ        /**< The IQ functionality is active */
  };

  class io_conf
  {
  public:
    bsp::spi               &spi_ctrl;
    bsp::gpio              &nreset;
    bsp::chrono            &chrono;
    rf_frontend09::io_conf &rffe09_io;
    rf_frontend24::io_conf &rffe24_io;
  };

  class fsk_conf
  {
  public:
    at86rf215_fsk_srate_t rate;
  };

  class rx_info
  {
  public:
    float     rssi;
    float     edv;
    uint32_t  len;
    interface iface;
  };

  class rx_msg
  {
  public:
    uint8_t pdu[AT86RF215_MAX_PDU];
    rx_info info;
  };

  /**
   * @brief Radio interface statistics
   *
   */
  class stats
  {
  public:
    size_t tx_frames;
    size_t tx_frames_fail;
    size_t tx_frames_drop;
    size_t rx_frames;
    size_t rx_frames_inval;
    size_t rx_frames_drop;

    stats()
        : tx_frames(0),
          tx_frames_fail(0),
          tx_frames_drop(0),
          rx_frames(0),
          rx_frames_inval(0),
          rx_frames_drop(0)
    {
    }
  };

  radio(const io_conf &cnf, power &pwr, bsp::imsgq<rx_msg> &rx_msgq);

  static void
  trx_irq_handler();

  void
  enable(bool yes = true);

  void
  enable(interface iface, bool yes = true);

  bool
  enabled() const;

  bool
  enabled(interface iface) const;

  void
  set_direction(interface iface, rf_frontend::dir d);

  rf_frontend::dir
  direction(interface iface) const;

  void
  set_frequency(interface iface, rf_frontend::dir d, float freq);

  float
  frequency(interface iface, rf_frontend::dir d) const;

  void
  set_test_fsk(interface iface);

  void
  tx(interface iface, const uint8_t *b, size_t len);

  void
  rx_async(interface iface);

  bool
  mixer_lock();

  rf_frontend09 &
  uhf();

  rf_frontend24 &
  sband();

  int
  recv_msg(rx_msg &msg, size_t timeout_ms);

  const stats &
  get_stats(interface iface) const;

private:
  /* IQIFSF TRXERR BATLOW EDC TRXRDY*/
  static constexpr uint8_t TX_RF_IRQM = 0xFE;
  /* All IRQs for the baseband core enabled during TX */
  static constexpr uint8_t TX_BB_IRQM = 0xFF;

  /* IQIFSF TRXERR BATLOW EDC TRXRDY*/
  static constexpr uint8_t RX_RF_IRQM = 0xFE;

  /* RXFE, RXFS */
  static constexpr uint8_t RX_BB_IRQM = 0x3;

  bsp::spi           &m_spi;
  bsp::gpio          &m_nrst;
  mutable struct at86rf215    m_at86;
  bool                m_iface_enabled[2];
  rf_frontend09       m_rffe09;
  rf_frontend24       m_rffe24;
  bsp::chrono        &m_chrono;
  power              &m_pwr;
  bsp::imsgq<rx_msg> &m_rxq;
  stats               m_stats[2];
  size_t              m_frame_size;

  void
  bsp_setup();

  void
  at86rf215_enable(bool enable = true);

  friend int
  at86rf215_irq_user_callback(struct at86rf215 *h, uint8_t rf09_irqs,
                              uint8_t rf24_irqs, uint8_t bbc0_irqs,
                              uint8_t bbc1_irqs);

  void
  incr_tx_frames(interface iface);

  void
  incr_tx_fail_frames(interface iface);

  void
  incr_tx_drop_frames(interface iface);

  void
  incr_rx_frames(interface iface);

  void
  incr_rx_inval_frames(interface iface);

  void
  incr_rx_drop_frames(interface iface);
};

} // namespace satnogs::comms

#endif // RADIO_HPP

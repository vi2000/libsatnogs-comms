/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef INA322X_HPP
#define INA322X_HPP

#include <cstdint>
#include <satnogs-comms/bsp/i2c.hpp>
#include <satnogs-comms/error.hpp>
#include <satnogs-comms/exception.hpp>

namespace satnogs::comms
{

class ina322x_comm_exception : public exception
{
public:
  ina322x_comm_exception(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("could not communicate with the INA322x IC",
                                 "ina322xcomm"),
                  file_name, line)
  {
  }
};

class ina322x
{
public:
  enum class channel : uint8_t
  {
    CH1 = 0,
    CH2 = 1,
    CH3 = 2
  };

  ina322x(bsp::i2c &i2c, uint16_t addr, float ch1_shunt = 0.1f,
          float ch2_shunt = 0.1f, float ch3_shunt = 0.1f);

  float
  current(channel ch) const;

  float
  voltage(channel ch) const;

  void
  reset();

private:
  bsp::i2c      &m_i2c;
  const uint16_t m_addr;
  const float    m_ch1_shunt;
  const float    m_ch2_shunt;
  const float    m_ch3_shunt;

  void
  write_reg(uint8_t reg, uint16_t value) const;

  uint16_t
  read_reg(uint8_t reg) const;
};

} // namespace satnogs::comms

#endif // INA322X_HPP

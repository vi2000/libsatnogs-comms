/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstdint>
#include <etl/string.h>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/bsp/spi.hpp>
#include <satnogs-comms/power.hpp>

namespace satnogs::comms
{
class fpga
{
public:
  enum class reg : uint32_t
  {
    id      = 0,
    version = 1,
    status  = 2
  };

  enum class hw : uint8_t
  {
    NONE,
    ALINX_AC7Z020,
    LIBREWAVE
  };

  fpga(hw h, bsp::spi &spi, power &pwr);

  void
  enable(bool en = true);

  bool
  enabled() const;

  void
  write_reg(uint32_t reg, uint32_t val);

  uint32_t
  read_reg(uint32_t reg);

  void
  write_reg(reg r, uint32_t val);

  uint32_t
  read_reg(reg r);

private:
  const hw        m_hw;
  etl::string<32> m_name;
  bsp::spi       &m_spi;
  power          &m_pwr;
};

} // namespace satnogs::comms

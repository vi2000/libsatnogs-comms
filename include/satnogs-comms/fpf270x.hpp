/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2021-2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef INCLUDE_SATNOGS_COMMS_FPF270X_HPP_
#define INCLUDE_SATNOGS_COMMS_FPF270X_HPP_

#include <etl/string.h>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/exception.hpp>

namespace satnogs::comms
{

class fpf270x_pin_not_configured_exception : public exception
{
public:
  fpf270x_pin_not_configured_exception(string_type file_name, numeric_type line)
      : exception(
            ETL_ERROR_TEXT("fpf270x pin is not configured", "fpf270xpincfg"),
            file_name, line)
  {
  }
};

class fpf270x
{
public:
  fpf270x(const etl::istring &name, bsp::gpio &on, bsp::gpio &pgood,
          bsp::gpio &flagb);

  const etl::istring &
  name() const;

  void
  enable(bool set = true);

  bool
  enabled() const;

  bool
  pgood() const;

  bool
  flagb() const;

private:
  etl::string<32> m_name;
  bsp::gpio      &m_on_gpio;
  bsp::gpio      &m_pgood_gpio;
  bsp::gpio      &m_flagb_gpio;
};

} // namespace satnogs::comms

#endif /* INCLUDE_SATNOGS_COMMS_FPF270X_HPP_ */

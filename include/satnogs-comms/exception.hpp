/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2021-2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef INCLUDE_SATNOGS_COMMS_EXCEPTION_HPP_
#define INCLUDE_SATNOGS_COMMS_EXCEPTION_HPP_

#include <etl/exception.h>
#include <etl/error_handler.h>

namespace satnogs::comms
{

class exception : public etl::exception
{
public:
  exception(const char *reason, const char *file, int lineno)
      : etl::exception(reason, file, lineno), m_has_errno(false), m_err(0)
  {
  }

  exception(const char *reason, const char *file, int lineno, int err)
      : etl::exception(reason, file, lineno), m_has_errno(true), m_err(err)
  {
  }

  /**
   *
   * @return the error number registered with the exception. If none was available
   * this method returns 0.
   */
  int
  err() const
  {
    return m_err;
  }

  /**
   *
   * @return true if the exception has an error code registered, false otherwise
   */
  bool
  has_errno() const
  {
    return m_has_errno;
  }

private:
  const bool m_has_errno;
  const int  m_err;
};

class inval_arg_exception : public exception
{
public:
  inval_arg_exception(string_type file_name, numeric_type line)
      : exception(
            ETL_ERROR_TEXT("Invalid argument", "invalarg"),
            file_name, line)
  {
  }
};

} /* namespace satnogs::comms */

#endif /* INCLUDE_SATNOGS_COMMS_EXCEPTION_HPP_ */

/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef EMC1702_H
#define EMC1702_H

#include <etl/string.h>
#include <etl/vector.h>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/bsp/i2c.hpp>
#include <satnogs-comms/error.hpp>
#include <satnogs-comms/exception.hpp>

namespace satnogs::comms
{
class emc1702
{
public:
  struct status
  {
    bool busy        = 0;
    bool crit        = 0;
    bool diode_fault = 0;
    bool low         = 0;
    bool high        = 0;
    bool peak        = 0;
  };
  enum class sensor_mode : uint8_t
  {
    FULLY_ACTIVE     = 0b00100000,
    CURRENT_ONLY     = 0b01100000,
    TEMPERATURE_ONLY = 0b00100100,
    STANDBY          = 0b01100100

  };

  enum class conversion_rate : uint8_t
  {
    PER_1_SEC_8_MEAS  = 0b00000111,
    PER_1_SEC_4_MEAS  = 0b00000110,
    PER_1_SEC_2_MEAS  = 0b00000101,
    PER_1_SEC_1_MEAS  = 0b00000100,
    PER_2_SEC_1_MEAS  = 0b00000011,
    PER_4_SEC_1_MEAS  = 0b00000010,
    PER_8_SEC_1_MEAS  = 0b00000001,
    PER_16_SEC_1_MEAS = 0b00000000
  };

  enum class consecutive_alert_diode_fault : uint8_t
  {
    OUT_OF_LIM_MEAS_1 = 0b00000000,
    OUT_OF_LIM_MEAS_2 = 0b00000010,
    OUT_OF_LIM_MEAS_3 = 0b00000110,
    OUT_OF_LIM_MEAS_4 = 0b00001110
  };

  enum class consecutive_alert_voltage : uint8_t
  {
    OUT_OF_LIM_MEAS_1 = 0b00000000,
    OUT_OF_LIM_MEAS_2 = 0b10000100,
    OUT_OF_LIM_MEAS_3 = 0b10001000,
    OUT_OF_LIM_MEAS_4 = 0b10001100
  };

  enum class consecutive_alert_current : uint8_t
  {
    OUT_OF_LIM_MEAS_1 = 0b00000000,
    OUT_OF_LIM_MEAS_2 = 0b01000000,
    OUT_OF_LIM_MEAS_3 = 0b10000000,
    OUT_OF_LIM_MEAS_4 = 0b11000000
  };

  enum class beta_config : uint8_t
  {
    MIN_BETA_0_050 = 0b00000000,
    MIN_BETA_0_066 = 0b00000001,
    MIN_BETA_0_087 = 0b00000010,
    MIN_BETA_0_114 = 0b00000011,
    MIN_BETA_0_150 = 0b00000100,
    MIN_BETA_0_197 = 0b00000101,
    MIN_BETA_0_260 = 0b00000110,
    MIN_BETA_0_342 = 0b00000111,
    MIN_BETA_0_449 = 0b00001000,
    MIN_BETA_0_591 = 0b00001001,
    MIN_BETA_0_778 = 0b00001010,
    MIN_BETA_1_024 = 0b00001011,
    MIN_BETA_1_348 = 0b00001100,
    MIN_BETA_1_773 = 0b00001101,
    MIN_BETA_2_333 = 0b00001110,
    DISABLED       = 0b00001111,
    AUTO           = 0b00010000
  };

  enum class ideality_factor : uint8_t
  {
    IDEALITY_FACTOR_1_0053 = 0b00010000,
    IDEALITY_FACTOR_1_0066 = 0b00010001,
    IDEALITY_FACTOR_1_0080 = 0b00010010,
    IDEALITY_FACTOR_1_0093 = 0b00010011,
    IDEALITY_FACTOR_1_0106 = 0b00010100,
    IDEALITY_FACTOR_1_0119 = 0b00010101,
    IDEALITY_FACTOR_1_0133 = 0b00010110,
    IDEALITY_FACTOR_1_0146 = 0b00010111
  };

  enum class averaging_control : uint8_t
  {
    DISABLED    = 0b00000000,
    AVERAGING2X = 0b00000001,
    AVERAGING4X = 0b00000010,
    AVERAGING8X = 0b00000011
  };

  enum class current_sampling_time : uint8_t
  {
    S_T_82_MS  = 0b00000000,
    S_T_164_MS = 0b00001000,
    S_T_328_MS = 0b00001100
  };

  enum class max_expected_voltage : uint8_t
  {
    CURRENT_SENSOR_RANGE_10_mV = 0b00000000,
    CURRENT_SENSOR_RANGE_20_mV = 0b00000001,
    CURRENT_SENSOR_RANGE_40_mV = 0b00000010,
    CURRENT_SENSOR_RANGE_80_mV = 0b00000011
  };

  enum class peak_detection_threshold : uint8_t
  {
    THRESHOLD_10_mV = 0b00000000,
    THRESHOLD_15_mV = 0b00010000,
    THRESHOLD_20_mV = 0b00100000,
    THRESHOLD_25_mV = 0b00110000,
    THRESHOLD_30_mV = 0b01000000,
    THRESHOLD_35_mV = 0b01010000,
    THRESHOLD_40_mV = 0b01100000,
    THRESHOLD_45_mV = 0b01110000,
    THRESHOLD_50_mV = 0b10000000,
    THRESHOLD_55_mV = 0b10010000,
    THRESHOLD_60_mV = 0b10100000,
    THRESHOLD_65_mV = 0b10110000,
    THRESHOLD_70_mV = 0b11000000,
    THRESHOLD_75_mV = 0b11010000,
    THRESHOLD_80_mV = 0b11100000,
    THRESHOLD_85_mV = 0b11110000
  };

  enum class peak_detection_duration : uint8_t
  {
    DURATION_1_00_ms    = 0b00000000,
    DURATION_5_12_ms    = 0b00000001,
    DURATION_25_60_ms   = 0b00000010,
    DURATION_51_20_ms   = 0b00000011,
    DURATION_76_80_ms   = 0b00000100,
    DURATION_102_40_ms  = 0b00000101,
    DURATION_128_00_ms  = 0b00000110,
    DURATION_256_00_ms  = 0b00000111,
    DURATION_384_00_ms  = 0b00001000,
    DURATION_512_00_ms  = 0b00001001,
    DURATION_768_00_ms  = 0b00001010,
    DURATION_1024_00_ms = 0b00001011,
    DURATION_1536_00_ms = 0b00001100,
    DURATION_2048_00_ms = 0b00001101,
    DURATION_3072_00_ms = 0b00001110,
    DURATION_4096_00_ms = 0b00001111
  };

  emc1702(const etl::istring &name, bsp::i2c &i2c, uint16_t addr,
          bsp::gpio &alert);

  emc1702(const etl::istring &name, bsp::i2c &i2c, uint16_t addr);

  void
  set_peak_detection_config(peak_detection_threshold t,
                            peak_detection_duration  d);

  void
  get_measurements(sensor_mode m);

  float
  get_temperature_internal() const;

  float
  get_temperature_external() const;

  void
  get_status(status &s);

  void
  set_config(sensor_mode m) const;

  void
  set_conversion_rate(conversion_rate r) const;

  void
  set_int_temp_high_lim(float int_temp_high_lim = 85.0f) const; // in °C

  void
  set_int_temp_low_lim(float int_temp_low_lim = -128.0f) const; // in °C

  void
  set_ext_temp_high_lim(float ext_temp_high_lim = 85.0f) const; // in °C

  void
  set_ext_temp_low_lim(float ext_temp_low_lim = -128.0f) const; // in °C

  void
  get_one_shot_meas() const;

  void
  set_t_crit(float ext_t_crit = 100.0f, float int_t_crit = 100.0f,
             float t_crit_hysterisis = 10.0f) const; // in °C

  bool
  get_diode_fault() const;

  void
  set_consecutive_alert(consecutive_alert_diode_fault c,
                        consecutive_alert_diode_fault l) const;

  void
  set_beta_configuration(beta_config b) const;

  void
  set_ideality_factor(ideality_factor i) const;

  etl::vector<bool, 4>
  get_high_limit_status() const;

  etl::vector<bool, 4>
  get_low_limit_status() const;

  bool
  get_crit_limit_status() const;

  void
  set_averaging_control(averaging_control a) const;

  bool
  get_sensor_info() const;

  void
  set_voltage_sampling_config(consecutive_alert_voltage v,
                              averaging_control         s) const;

  void
  set_current_sense_sampling_config(consecutive_alert_current v,
                                    averaging_control         i,
                                    current_sampling_time     t,
                                    max_expected_voltage      m) const;

  void
  set_peak_detection_config(peak_detection_threshold t,
                            peak_detection_duration  d) const;

  float
  get_sense_current() const;

  float
  get_source_voltage() const;

  float
  get_power() const;

  void
  set_v_sense_lim(float v_sense_lim_low  = 2032.0f,
                  float v_sense_lim_high = -2033.0f) const; //in mV
  // Bear in mind that the LSB in this value is 16, and the value is signed and 8-bit

  void
  set_v_source_lim(float v_source_lim_low  = 0.0f,
                   float v_source_lim_high = 23.9063f) const; // in V

  void
  set_v_crit_lim(float v_sense_lim_crit    = 2032.0f,        // in mV
                 float v_source_lim_crit   = 23.9063f,       // in V
                 float v_sense_hysterisis  = 160.0f,         // in mV
                 float v_source_hysterisis = 0.9375f) const; // in V

  bool
  alert() const;

  float
  get_temperature_average() const; // in °C

private:
  etl::string<32>               m_name;
  bsp::i2c                     &m_i2c;
  const uint32_t                m_addr;
  bsp::gpio                    &m_alert;
  sensor_mode                   mode;
  conversion_rate               rate;
  averaging_control             avrg_temp;
  consecutive_alert_diode_fault alert_crit;
  consecutive_alert_diode_fault alert_lim;
  beta_config                   beta;
  ideality_factor               i_factor;
  consecutive_alert_voltage     v_source_spike;
  averaging_control             avrg_v_source;
  consecutive_alert_current     v_sense_spike;
  averaging_control             avrg_i_sense;
  current_sampling_time         i_sampling;
  max_expected_voltage          v_sense_max;
  peak_detection_threshold      thres;
  peak_detection_duration       dur;
  max_expected_voltage          range;
  status                        s;
  bsp::dummy_gpio               m_dummy;
};

class emc1702_inval_param_exception : public exception
{
public:
  emc1702_inval_param_exception(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("emc1702 invalid param", "emc1702invalparam"),
                  file_name, line)
  {
  }
};

class emc1702_thermal_shutdown_needed : public exception
{
public:
  emc1702_thermal_shutdown_needed(string_type file_name, numeric_type line)
      : exception(
            ETL_ERROR_TEXT("Thermal Shutdon is required", "thermalshutdown"),
            file_name, line)
  {
  }
};

class emc1702_incorrect_sensor_info : public exception
{
public:
  emc1702_incorrect_sensor_info(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("Error in getting sensor info. Probably there "
                                 "is an error in communication",
                                 "incorrectinfo"),
                  file_name, line)
  {
  }
};

class emc1702_not_ready : public exception
{
public:
  emc1702_not_ready(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("The ADCs currently converting", "busy"),
                  file_name, line)
  {
  }
};

} // namespace satnogs::comms

#endif // EMC1702_H

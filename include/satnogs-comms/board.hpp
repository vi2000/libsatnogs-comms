/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <type_traits>

#include <satnogs-comms/bsp/adc.hpp>
#include <satnogs-comms/bsp/chrono.hpp>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/bsp/i2c.hpp>
#include <satnogs-comms/bsp/spi.hpp>
#include <satnogs-comms/emc1702.hpp>
#include <satnogs-comms/emmc.hpp>
#include <satnogs-comms/fpga.hpp>
#include <satnogs-comms/leds.hpp>
#include <satnogs-comms/power.hpp>
#include <satnogs-comms/radio.hpp>
#include <satnogs-comms/temperature.hpp>
#include <satnogs-comms/tmp117.hpp>
#include <satnogs-comms/version.hpp>

namespace satnogs::comms
{

class board
{
public:
  class io_conf
  {
  public:
    bsp::gpio      &emmc_en;
    bsp::gpio      &emmc_sel;
    bsp::spi       &fpga_spi;
    bsp::gpio      &led0;
    bsp::gpio      &led1;
    power::io_conf &pwr_io;
    bsp::i2c       &sensors_i2c;
    bsp::gpio      &alert_t_pa_uhf;
    bsp::gpio      &alert_t_pa_sband;
    radio::io_conf &radio_io;
    bsp::chrono    &chrono;
  };

  class params
  {
  public:
    fpga::hw                  fpga_hw;
    uint64_t                  emmc_capacity;
    uint64_t                  emmc_start;
    uint64_t                  emmc_stop;
    bsp::imsgq<radio::rx_msg> &rx_msgq;
  };

  static void
  init(const io_conf &io, const params &p);

  /* Singleton */
  static board &
  get_instance();

  board(board const &) = delete;

  void
  operator=(board const &) = delete;

  comms::radio &
  radio();

  comms::emmc &
  emmc();

  comms::power &
  power();

  comms::fpga &
  fpga();

  comms::leds &
  leds();

  bool
  alert(temperature_sensor s) const;

  float
  temperature(temperature_sensor s) const;

protected:
  board(const io_conf &io, const params &p);

private:
  /* Select the proper temperature sensor class based on the hardware version */
  using temp_driver =
      typename std::conditional_t<(version::hw() > version::num(0, 2)) == true,
                                  emc1702, tmp117>;
  bool m_init;
  /*
   * power subsystem should be first, because many of the other subsystems
   * depends on it
   */
  comms::power                    m_power;
  comms::radio                    m_radio;
  comms::fpga                     m_fpga;
  comms::emmc                     m_emmc;
  comms::leds                     m_leds;
  comms::temperature<temp_driver> m_temperature;
};

class board_uninitialized_exception : public exception
{
public:
  board_uninitialized_exception(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("board not initialized (init() not called)",
                                 "brdnoinit"),
                  file_name, line)
  {
  }
};

class board_initialized_exception : public exception
{
public:
  board_initialized_exception(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("board already initialized", "brdinit"),
                  file_name, line)
  {
  }
};

} // namespace satnogs::comms

/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef GPIO_HPP
#define GPIO_HPP

#include <cstdint>

namespace satnogs::comms::bsp
{

/**
 * @brief Generic GPIO virtual class
 *
 */
class gpio
{
public:
  enum class direction : uint8_t
  {
    INPUT  = 0,
    OUTPUT = 1
  };

  gpio(direction dir = direction::INPUT) {}

  /**
   * @brief Toggles the GPIO pin if it is configured as output.
   * Has no effect if it is conigured as input
   *
   */
  virtual void
  toggle() = 0;

  /**
   * @brief Gets the logical level of the GPIO pin. For example, if the pin
   * has been configured as active low, and the input level is 0V, this method
   * will return true
   *
   * @return true if the logical level is 1
   * @return false if the logical level is 0
   */
  virtual bool
  get() = 0;

  /**
   * @brief Sets the logical output of the pin. For example, if the pin
   * has been configured as active low, setting \p s to true will set
   * the output pin to 0V.
   *
   * Has no effect if the pin is not configured as output.
   *
   * @param s the logical level of the output pin
   */
  virtual void
  set(bool s) = 0;

  /**
   * @brief Set the direction of the GPIO
   *
   * @param dir the desired direction
   */
  virtual void
  set_direction(direction dir) = 0;
};

/**
 * @brief A dummy GPIO implementation with no effect.
 * Can be used for CI testing or default construction of gpio objects
 */
class dummy_gpio : public gpio
{
public:
  dummy_gpio(){};

  void
  toggle()
  {
  }

  bool
  get()
  {
    return false;
  }

  void
  set(bool s)
  {
  }

  void
  set_direction(direction dir)
  {
  }
};

} // namespace satnogs::comms::bsp

#endif // GPIO_HPP

/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef ADC_HPP
#define ADC_HPP

#include <cstdint>

namespace satnogs::comms::bsp
{

/**
 * @brief Generic ADC virtual class
 *
 */
class adc
{
public:
  /**
   * @brief Construct a new ADC object
   *
   * @param resolution the ADC resolution
   * @param vref the reference voltage of the ADC in Volts
   */
  adc(uint16_t resolution, float vref) : m_resolution(resolution), m_vref(vref)
  {
  }

  /**
   * @brief Starts the ADC
   *
   */
  virtual void
  start() = 0;

  /**
   * @brief Stops the ADC
   *
   */
  virtual void
  stop() = 0;

  /**
   * @brief Get the ADC value
   *
   * @return uint32_t the ADC latest conversion
   */
  virtual uint32_t
  read() = 0;

  virtual float
  voltage()
  {
    return read() * vref() / (1UL << m_resolution);
  }

  uint32_t
  resolution() const
  {
    return m_resolution;
  }

  /**
   *
   * @return float the reference voltage in Volts
   */
  float
  vref() const
  {
    return m_vref;
  }

  /**
   * @brief Updates the reference voltage
   *
   * @param vref the reference voltage in Volts
   */
  void
  set_vref(float vref)
  {
    m_vref = vref;
  }

private:
  const uint32_t m_resolution;
  float          m_vref;
};

class dummy_adc : public adc
{
};

} // namespace satnogs::comms::bsp

#endif // ADC_HPP

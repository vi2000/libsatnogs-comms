/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef SPI_HPP
#define SPI_HPP

#include "gpio.hpp"
#include <cstddef>
#include <cstdint>

namespace satnogs::comms::bsp
{
class spi
{
public:
  spi() {}
  ~spi() {}

  /**
   * @brief Performs an SPI read operation
   *
   * @param rx the receive buffer. Should be at least \p len bytes in size
   * @param tx the transmit buffer. Should be at least \p len bytes in size
   * @param tx_len number of bytes to be transmitted
   * @param rx_len number of bytes to be received
   */
  virtual void
  read(uint8_t *rx, const uint8_t *tx, size_t tx_len, size_t rx_len);

  /**
   * @brief Performs an SPI write operation
   *
   * @param tx the transmit buffer. Should be at least \p len bytes in size
   * @param len number of bytes to be transmitted
   */
  virtual void
  write(const uint8_t *tx, size_t len);

  /**
   * @brief Returns a GPIO control handle for the CS of the SPI. If this is performed
   * automatically by the hardware, users does not have to provide an implementation
   * for this method, as it returns an object of \ref dummy_gpio class, which has no
   * effect
   *
   * @return gpio&
   */
  virtual gpio &
  cs()
  {
    return m_cs;
  }

private:
  dummy_gpio m_cs;
};

} // namespace satnogs::comms::bsp

#endif // SPI_HPP

/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef CHRONO_HPP
#define CHRONO_HPP

#include <cstddef>
#include <cstdint>

namespace satnogs::comms::bsp
{
class chrono
{
public:
  chrono() {}
  ~chrono() {}

  /**
   * @brief Delays the execution of the active task for at least \p us microseconds
   * @note On bare metal execution environments this can be a busy-wait loop
   * @param us the amount of microseconds to delay the execution
   */
  virtual void
  delay_us(size_t us);

  /**
   *
   * @return int64_t with the current time in milliseconds
   */
  virtual int64_t
  time_ms();
};

} // namespace satnogs::comms::bsp

#endif // CHRONO_HPP

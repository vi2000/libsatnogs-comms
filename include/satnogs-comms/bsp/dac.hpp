/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef DAC_HPP
#define DAC_HPP

#include <cstdint>

namespace satnogs::comms::bsp
{

class dac
{
public:
  dac() {}

  virtual void
  start() = 0;
  virtual void
  stop() = 0;
  virtual void
  write(uint32_t x) = 0;

private:
};

} // namespace satnogs::comms::bsp

#endif // DAC_HPP

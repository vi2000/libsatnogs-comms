/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <cstdint>
#include <cstdlib>

namespace satnogs::comms::bsp
{

template <typename T> class imsgq
{
public:
  imsgq(size_t len) : m_len(len) {}

  imsgq(const imsgq &) = delete;

  size_t
  max_size() const
  {
    return m_len;
  }

  virtual size_t
  size() = 0;

  virtual int
  put(const T &msg, size_t timeout_ms) = 0;

  virtual int
  put_isr(const T &msg) = 0;

  virtual int
  peek(T *msg) = 0;

  virtual int
  get(T *msg, size_t timeout_ms) = 0;

  virtual int
  get_isr(T *msg) = 0;

protected:
  const size_t m_len;
};

template <typename T, const size_t LEN> class msgq : public imsgq<T>
{
public:
  msgq() : imsgq<T>(LEN) {}
};
} // namespace satnogs::comms::bsp

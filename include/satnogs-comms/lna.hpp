/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2021-2022, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef INCLUDE_SATNOGS_COMMS_LNA_HPP_
#define INCLUDE_SATNOGS_COMMS_LNA_HPP_

#include <etl/string.h>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/exception.hpp>

namespace satnogs::comms
{

class lna
{
public:
  lna(const etl::istring &name, bsp::gpio &io);

  const etl::istring &
  name() const;

  void
  enable(bool set = true);

  void
  toggle();

  bool
  enabled() const;

protected:
  bsp::gpio &m_gpio;

private:
  etl::string<32> m_name;
};

} // namespace satnogs::comms

#endif /* INCLUDE_SATNOGS_COMMS_LNA_HPP_ */

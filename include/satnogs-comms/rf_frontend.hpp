/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef INCLUDE_SATNOGS_COMMS_RF_FRONTEND_HPP_
#define INCLUDE_SATNOGS_COMMS_RF_FRONTEND_HPP_

#include <etl/string.h>
#include <satnogs-comms/ad8318.hpp>
#include <satnogs-comms/bsp/adc.hpp>
#include <satnogs-comms/bsp/dac.hpp>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/exception.hpp>
#include <satnogs-comms/f2972.hpp>
#include <satnogs-comms/power.hpp>
#include <utility>

namespace satnogs::comms
{

class rf_frontend_inval_param_exception : public exception
{
public:
  rf_frontend_inval_param_exception(string_type file_name, numeric_type line)
      : exception(ETL_ERROR_TEXT("rf_frontend invalid param", "rfinvalparam"),
                  file_name, line)
  {
  }
};

class rf_frontend
{
public:
  class io_conf
  {
  public:
    bsp::gpio &en_agc;
    bsp::dac  &agc_vset;
    bsp::gpio &flt_sel;
  };

  /**
   * Direction / interface
   */
  enum class dir : uint8_t
  {
    RX, /**< RX interface */
    TX  /**< TX interface */
  };

  /**
   * RX Gain mode
   */
  enum class gain_mode : uint8_t
  {
    AUTO,  /**< AGC enabled */
    MANUAL /**< AGC bypassed */
  };

  enum class filter : uint8_t
  {
    WIDE,
    NARROW
  };

  rf_frontend(const std::pair<float, float> &rx_range,
              const std::pair<float, float> &tx_range, io_conf &&io,
              power &pwr);

  /**
   * Enable/Disable the RF frontend and its associated components
   * @param set true to enable, false to disable
   */
  virtual void
  enable(bool set = true) = 0;

  virtual bool
  enabled() const = 0;

  virtual void
  set_filter(filter f);

  virtual filter
  get_filter() const;

  virtual void
  set_direction(dir d);

  dir
  direction() const;

  void
  set_gain_mode(enum gain_mode mode);

  enum gain_mode
  gain_mode() const;

  virtual void
  set_gain(float db);

  virtual float
  gain() const;

  bool
  frequency_valid(dir d, float freq) const;

protected:
  std::pair<float, float> m_rx_freq_range;
  std::pair<float, float> m_tx_freq_range;
  bsp::gpio              &m_filt_sel;
  power                  &m_pwr;
  ad8318                  m_agc;
  dir                     m_dir;
  enum gain_mode          m_gain_mode;
  float                   m_gain;
};

} // namespace satnogs::comms

#endif /* INCLUDE_SATNOGS_COMMS_RF_FRONTEND_HPP_ */

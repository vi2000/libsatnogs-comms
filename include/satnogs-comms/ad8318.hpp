/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef INCLUDE_SATNOGS_COMMS_AD8318_HPP_
#define INCLUDE_SATNOGS_COMMS_AD8318_HPP_

#include <etl/string.h>
#include <satnogs-comms/bsp/adc.hpp>
#include <satnogs-comms/bsp/dac.hpp>
#include <satnogs-comms/bsp/gpio.hpp>

namespace satnogs::comms
{

class ad8318
{
public:

  ad8318(bsp::gpio &en, bsp::dac &vset);

  void
  enable(bool set = true);

  void
  toggle();

  bool
  enabled() const;

  void
  vset(float val);

private:
  bsp::gpio &m_en_gpio;
  bsp::dac  &m_vset;
};

} // namespace satnogs::comms

#endif /* INCLUDE_SATNOGS_COMMS_AD8318_HPP_ */

# ##############################################################################
# SatNOGS-COMMS control library
#
# Copyright (C) 2024, Libre Space Foundation <http://libre.space>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GNU General Public License v3.0 or later
# ##############################################################################

#
# Sets the HW_VERSION_MAJOR, HW_VERSION_MINOR and HW_VERSION_PATCH from a
# version string following the major.minor<.patch> versioning scheme. If the
# patch is ommited, it is considered as 0.
#
# Usage: libsatnogs_comms_set_hw(VERSION "major.minor.patch")
#
function(libsatnogs_comms_set_hw)
  set(oneval VERSION)
  cmake_parse_arguments(PARSED_ARGS "" "${oneval}" "" ${ARGN})
  if(NOT DEFINED PARSED_ARGS_VERSION)
    message(FATAL_ERROR "VERSION parameter not set")
  endif()

  # Extract the major and minor version from the string
  string(REPLACE "." ";" HW_VERSION_LIST ${PARSED_ARGS_VERSION})
  list(LENGTH HW_VERSION_LIST HW_VERSION_LIST_LEN)
  list(GET HW_VERSION_LIST 0 HW_VERSION_MAJOR)
  list(GET HW_VERSION_LIST 1 HW_VERSION_MINOR)
  if(${HW_VERSION_LIST_LEN} GREATER 2)
    list(GET HW_VERSION_LIST 2 HW_VERSION_PATCH)
  else()
    set(HW_VERSION_PATCH 0)
  endif()

  if((PARSED_ARGS_VERSION VERSION_GREATER "0.3") OR (PARSED_ARGS_VERSION
                                                     VERSION_LESS "0.2"))
    message(FATAL_ERROR "Hardware versions 0.2 - 0.3 are supported")
  endif()

  message(
    STATUS
      "Building libsatnogs-comms ${PROJECT_VERSION} for PCB version"
      " ${HW_VERSION_MAJOR}.${HW_VERSION_MINOR}.${HW_VERSION_PATH}"
  )

  set(HW_VERSION_MAJOR
      ${HW_VERSION_MAJOR}
      PARENT_SCOPE)
  set(HW_VERSION_MINOR
      ${HW_VERSION_MINOR}
      PARENT_SCOPE)
  set(HW_VERSION_PATCH
      ${HW_VERSION_PATCH}
      PARENT_SCOPE)
endfunction()
